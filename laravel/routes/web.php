<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home/');
});

Route::get('/home', 'PostController@home')->middleware('user_auth');
Route::get('/draft', 'PostController@draft')->middleware('user_auth');
Route::get('/edit/{id}', 'PostController@editPost')->middleware('user_auth');
Route::get('/top', 'PostController@TopPost')->middleware('user_auth');
Route::post('/home2', 'PostController@home2');
Route::get('/login', 'AuthController@login');
Route::get('/landing', 'AuthController@landing');
Route::post('/logout', 'AuthController@do_logout');
Route::get('/register', 'AuthController@register');
Route::post('/do_login', 'AuthController@do_login');
Route::post('/do_register', 'AuthController@do_register');
Route::get('/bookmark', 'PostController@getBookmarkPost')->middleware('user_auth');
Route::get('/search', 'ProfileController@search')->middleware('user_auth');
Route::post('/do_search', 'ProfileController@do_search');

Route::get('/create/{kategori}', 'PostController@createPost')->middleware('user_auth');
Route::post('/create/insert', 'PostController@savePost')->middleware('user_auth');
Route::post('/create/update', 'PostController@updatePost')->middleware('user_auth');

Route::post('/post/upload/attachment', 'PostController@uploadAttachment');
Route::post('/post/upload/image', 'PostController@uploadImage');
Route::post('/post/add/comment', 'PostController@addComment');
Route::post('/post/love_unlove', 'PostController@loveUnlove');
Route::post('/post/set_bookmark', 'PostController@bookmarkUnbookmark');
Route::post('/post/delete','PostController@deletePost');

Route::get('/post/detail/{id}','PostController@detailPost');

Route::get('/profile/edit', 'ProfileController@edit')->middleware('user_auth');
Route::post('/profile/update', 'ProfileController@update');
Route::post('/login/social', 'AuthController@loginWithSocial');
Route::get('/profile/follower/{id}', 'ProfileController@follower')->middleware('user_auth');
Route::get('/profile/following/{id}', 'ProfileController@following')->middleware('user_auth');
Route::post('/profile/follow', 'ProfileController@follow')->middleware('user_auth');
Route::get('/notifikasi', 'ProfileController@notifikasi')->middleware('user_auth')->middleware("read_notif");
Route::get('/notifikasi2', 'ProfileController@notifikasi2')->middleware('user_auth');
Route::post('/notifikasi/truncate', 'ProfileController@truncateNotif')->middleware('user_auth');
Route::get('/profile/{UserId}/{ViewerId}', 'ProfileController@index')->middleware('user_auth');
Route::get('/help', 'ProfileController@help')->middleware('user_auth');

//Auth::routes();
