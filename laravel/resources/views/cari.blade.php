@extends("templates/template")
@section("content")
<script src="{{ asset('node_modules/jquery/dist/jquery.min.js')}}"></script> 
  <!-- Blog Entries Column -->
  <div class="col-md-2"></div>
  <div class="col-md-8">
      <br/>
      <div class="card mb-4">
          <div class="card-body">
                <div class="form-group">
                    <input type="text" class="form-control" id="pencarian" aria-describedby="emailHelp" placeholder="Masukkan nama">
                </div>
                <div id="data" class="col-md-12">

                </div>
          </div>
      </div>
  </div>
  <div class="col-md-2"></div>
  <script>
      $('#pencarian').on('keyup',function() {
        var idq = {{session("id")}};
        var query = $(this).val();
        $.ajax({
            url: '/nightblog/do_search',
            type: 'POST', 
            data: {
                query:query
            },
            success : function(response){
              console.log(response);
              $("#data").html("");
              for(i=0;i<10;i++){
                  $("#data").append("<li onclick=\"location.href='/nightblog/profile/"+response[i].id+"/"+idq+"'\"><img style=\"width:50px;height:50px;border-radius:50%;margin-top:5px;margin-right:5px\" onError=\"this.onerror=null;this.src='https://demo.sukowidodo.com/nightblog/public/my_template/image/logo.jpeg';\" src='"+response[i].pic+"'/>"+response[i].name+"</li>")
              }
              
            },
          });
      });
  </script>
@endsection