@extends('templates/template')
@section('content')
    <style>
    .btn-success {
        font-family: Raleway-SemiBold;
        font-size: 8px;
        color: #FFFFFF;
        letter-spacing: 1px;
        line-height: 15px;
        border: 2px solid #FFFFFF;
        border-radius: 40px;
        background: transparent;
        transition: all 0.3s ease 0s;
        float:right;
        margin-right:5px;
        padding:10px;
        }

        .card-header {
          padding: .75rem 1.25rem;
          margin-bottom: 0;
          background-color: #fff;
          border-bottom: 0px solid rgba(0,0,0,.12);
      }

      .three-lines {
          -webkit-line-clamp: 3;
          display: -webkit-box;
          -webkit-box-orient: vertical;
          white-space: normal;
      }

      .two-lines {
          -webkit-line-clamp: 2;
          display: -webkit-box;
          -webkit-box-orient: vertical;
          white-space: normal;
          overflow: hidden;
      }

      .pl-3, .px-3 {
            padding-left: 10px!important;
        }
        .pb-3, .py-3 {
            padding-bottom: 0rem!important;
        }
        .pr-3, .px-3 {
            padding-right: 10px!important;
        }
        .pt-3, .py-3 {
            padding-top: 1rem!important;
        }

        .container {
            width: 100%;
            padding-right: 0px;
            padding-left: 0px;
            margin-right: auto;
            margin-left: auto;
        }
    </style>
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="card mb-4">
            <div style="height:400px;background:#263238" class="text-center">
               <br>
               <div class="row">
               <div class="col-12">
                   <div style="float:right">
                        @if($isFollow == 1)
                            <span onclick="followUnfollow({{$profile->id}}, false)" style="float:right;margin-right:30px;color:#FFFFFF"><img style="width:30px;height:30px" src="{{ asset('my_template/image/icon_follow_centang.png')}}"></span>
                        @elseif($isFollow == 0)
                            <span onclick="followUnfollow({{$profile->id}}, true)" style="float:right;margin-right:30px;color:#FFFFFF"><img style="width:30px;height:30px" src="{{ asset('my_template/image/icon_follow_add.png')}}"></span>
                        @else
                    
                        <span style="margin-right:30px;color:#FFFFFF">
                            <i class="fa fa-pencil fa-lg" aria-hidden="true" onclick="location.href='/nightblog/profile/edit';"></i>
                        </span> 
                        <span style="margin-right:30px;color:#FFFFFF">
                            <i class="fa fa-sign-out fa-lg" aria-hidden="true" onclick="signOut();"></i>
                        </span> 
                        @endif
                   </div>
               </div>
                
                <div class="col-4" style="padding-top:150px">
                    <div class="col-12" style="color:#FFFFFF">
                        <div style="float:left;margin-left:0px">@if(!empty($profile->profesi))<i class="fa fa-suitcase" aria-hidden="true" style="margin-right:10px"></i> {{ $profile->profesi }}@endif </div>
                    </div>
                    <br>
                   <div class="col-12" style="color:#FFFFFF">
                        <div style="float:left;margin-left:0px">
                            @if(!empty($profile->profesi))<i class="fa fa-map-marker" aria-hidden="true" style="margin-right:10px"></i> {{ $profile->lokasi }} @endif</div>
                   </div>
                </div>
                <div class="col-4">
                    <?php 
                    if(empty($profile->pic)){
                        if ($profile->password == "3667b4d8fffc4a51b5fa71ff226e4181") {
                            $profile->pic = "https://graph.facebook.com/".$profile->username."/picture?type=large";
                        } else if ($profile->password == "b49179f14eb706bfeb6a18bb771b493f") {
                            $profile->pic = $profile->username;
                        }else{
                            $profile->pic = "https://demo.sukowidodo.com/nightblog/public/my_template/image/logo.jpeg";
                        }
                    }else if(substr($profile->pic,0,4)=="http"){
                        $profile->pic = $profile->pic;
                    }   
                    ?>
                    <img onError="this.onerror=null;this.src='https://demo.sukowidodo.com/nightblog/public/my_template/image/logo.jpeg';" src="{{$profile->pic}}" class="circle mx-auto d-block" style="border-radius:50%;margin-top:80px;width:120px;height:120px"/>
                </div>
                <div class="col-4" style="padding-top:100px">
                    <div class="col-12">
                        <div class="btn-success" onclick="location.href='/nightblog/profile/follower/{{$profile->id}}';">Follower <br> {{ $profile->Followers }}</div>
                   </div>
                    <div class="col-12">
                        <div class="btn-success" style="margin-top:10px" onclick="location.href='/nightblog/profile/following/{{$profile->id}}';">Following <br> {{ $profile->Following }}</div>
                   </div>
                </div>
                </div>
                <div style="color:#FFFFFF; margin-top:30px;float:left;margin-left:20px;"><h2>{{$profile->name}}</h2></div>
            </div>

            <div class="card-body" style="padding:0px">
                <nav style="background:#263238">
                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" style="color:#FFFFFF" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Karya Tulis</a>
                        <a class="nav-item nav-link" style="color:#FFFFFF" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Ulasan</a>
                    </div>
                </nav>
                <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        @if($profile->islocked==1 && $isFollow==0)    
                            <div class="card-body" style="text-align:center;height:300px;padding-top:100px">    
                                <i class="fa fa-lock fa-4x" aria-hidden="true"></i>
                            </div>
                        @else
                            @foreach ($karya as $karya)
                            <div class="card mb-4">
                                <div class="card-header">
                                    @if(!empty($karya->pic))
                                    <span style="float:left"><img onError="this.onerror=null;this.src='https://demo.sukowidodo.com/nightblog/public/my_template/image/logo.jpeg';" src="{{ str_replace("http://res.cloudinary.com/nightblog/image/upload/w_300,h_300","http://res.cloudinary.com/nightblog/image/upload/w_600,h_600",$karya->pic)}}" class="img img-circle" style="border-radius:50%;width:50px;50px" alt="sdss"/> </span>
                                    <span style="float:left;margin-top:10px;margin-left:20px;color:#3297fc"><b>{{$karya->UserName}}</b></span>
                                    @else
                                    <span style="float:left"><img src="{{ asset('my_template/image/logo.jpeg')}}" class="img img-circle" style="float:left;margin-top:10px;margin-left:20px" alt="sdss"/> </span>
                                    <span style="margin-top:10px;margin-left:20px;color:#3297fc"><b>{{$karya->UserName}}</b></span>
                                    @endif
                                    @if($karya->IsBookmark==1)
                                    <span style="float:right;margin-top:10px;margin-left:10px;" onclick="bookMark({{$karya->PostId}},true)"><i class="fa fa-bookmark fa-2x"></i></span>
                                    @else
                                    <span style="float:right;margin-top:10px;margin-left:10px;" onclick="bookMark({{$karya->PostId}},false)"><i class="fa fa-bookmark-o fa-2x"></i></span>
                                    @endif
                                </div>
                                <div class="card-body" onclick="location.href='/nightblog/post/detail/{{$karya->PostId}}';">
                                    <div class="row">
                                        @if ($karya->Cover)
                                            <div class="col-8">
                                        @else
                                            <div class="col-12">
                                        @endif
                                            <h2 class="card-title two-lines"><b>{{ $karya->Title }}</b></h2>
                                            <p class="card-title"><b>{{ "(".$karya->CategoryName.")" }}</b></p>
                                            <p class="card-text three-lines" ><?php echo string(strip_tags(htmlspecialchars_decode($karya->Content), '<p>'))->tease(50); ?></p>
                                        </div>
                                        @if ($karya->Cover)
                                            <div class="col-4">   
                                                    <img class="card-img-top thumb-post img-fluid" src="{{"http://res.cloudinary.com/nightblog/image/upload/w_300,h_300,c_fill/".$karya->Cover}}" styles="height:100px;">
                                                {{-- @else
                                                    <img class="card-img-top thumb-post img-fluid" src="http://placehold.it/750x300" styles="height:100px;"> --}}
                                                
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer ">
                                    @if($karya->IsLove == 1)
                                    <i class="fa fa-heart text-danger" aria-hidden="true" onclick="loveUnlove({{$karya->PostId}},true)"></i> 
                                    @else
                                    <i class="fa fa-heart-o" aria-hidden="true" onclick="loveUnlove({{$karya->PostId}},false)"></i>
                                    @endif
                                    {{$karya->LoveCount}}
                                    <i class="fa fa-comment text-success" aria-hidden="true" style="margin-left:5px" ></i> {{$karya->CommentCount}}
                                    <?php
                                        $format = 'Y-m-d H:i:s';
                                        $date = DateTime::createFromFormat($format, $karya->PublishDate);
                                        $date = $date->format('j M Y');
                                    ?>
                                    <span style="float:right;margin-left:10px;"><i class="fa fa-clock-o" aria-hidden="true"></i> {{$date}}</span>
                                </div>
                            </div>
                            @endforeach
                        @endif
                    </div>

                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        @if($profile->islocked==1 && $isFollow==0)    
                            <div class="card-body" style="text-align:center;height:300px;padding-top:100px">    
                                <i class="fa fa-lock fa-4x" aria-hidden="true"></i>
                            </div>
                        @else
                            @foreach ($ulasan as $ulasan)
                            <div class="card mb-4">
                                <div class="card-header">
                                @if(!empty($ulasan->pic))
                                    <span style="float:left"><img onError="this.onerror=null;this.src='https://demo.sukowidodo.com/nightblog/public/my_template/image/logo.jpeg';" src="{{ str_replace("http://res.cloudinary.com/nightblog/image/upload/w_300,h_300","http://res.cloudinary.com/nightblog/image/upload/w_600,h_600",$ulasan->pic)}}" class="img img-circle" style="border-radius:50%;width:50px;height:50px" alt="sdss"/> </span>
                                    <span style="float:left;margin-top:10px;margin-left:20px;color:#3297fc"><b>{{$ulasan->UserName}}</b></span>
                                    @else
                                    <span style="float:left"><img src="{{ asset('my_template/image/logo.jpeg')}}" class="img img-circle" style="border-radius:50%;width:30px;height:30px" alt="sdss"/> </span>
                                    <span style="float:left;margin-top:10px;margin-left:20px;color:#3297fc"><b>{{$karya->UserName}}</b></span>
                                    @endif
                                    @if($ulasan->IsBookmark==1)
                                    <span style="float:right;margin-top:10px;margin-left:10px;" onclick="bookMark({{$ulasan->PostId}},true)"><i class="fa fa-bookmark fa-2x"></i></span>
                                    @else
                                    <span style="float:right;margin-top:10px;margin-left:10px;" onclick="bookMark({{$ulasan->PostId}},false)"><i class="fa fa-bookmark-o fa-2x"></i></span>
                                    @endif
                                </div>
                                <div class="card-body" onclick="location.href='/nightblog/post/detail/{{$ulasan->PostId}}';">
                                    <div class="row">
                                        <div class="col-8">
                                            <h2 class="card-title two-lines"><b>{{ $ulasan->Title }}</b></h2>
                                            <p class="card-title"><b>{{ "(".$ulasan->CategoryName.")" }}</b></p>
                                            <p class="card-text three-lines" ><?php echo string(strip_tags(htmlspecialchars_decode($ulasan->Content), '<p>'))->tease(50); ?></p>
                                        </div>
                                        @if ($ulasan->Cover)
                                            <div class="col-4">
                                                <img class="card-img-top thumb-post img-fluid" src="{{"http://res.cloudinary.com/nightblog/image/upload/w_300,h_300,c_fill/".$ulasan->Cover}}" styles="height:100px;">
                                            {{-- @else
                                                <img class="card-img-top thumb-post img-fluid" src="http://placehold.it/750x300" styles="height:100px;"> --}}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer ">
                                    @if($ulasan->IsLove == 1)
                                    <i class="fa fa-heart text-danger" aria-hidden="true" onclick="loveUnlove({{$ulasan->PostId}},true)"></i> 
                                    @else
                                    <i class="fa fa-heart-o" aria-hidden="true" onclick="loveUnlove({{$ulasan->PostId}},false)"></i>
                                    @endif
                                    {{$ulasan->LoveCount}}
                                    
                                    <i class="fa fa-comment text-success" aria-hidden="true" style="margin-left:5px" ></i> {{$ulasan->CommentCount}}
                                    <?php
                                        $format = 'Y-m-d H:i:s';
                                        $date = DateTime::createFromFormat($format, $ulasan->PublishDate);
                                        $date = $date->format('j M Y');
                                    ?>
                                    <span style="float:right;margin-left:10px;"><i class="fa fa-clock-o" aria-hidden="true"></i> {{$date}}</span>
                                </div>
                            </div>
                            @endforeach

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2"></div>
    <script>
        
        //$('#loader').hide();
        function followUnfollow(id,isFollow){
            console.log(isFollow);
            $.ajax({
                    url: '/nightblog/profile/follow',
                    type: 'POST', 
                    data: {
                        user_id:id,
                        follower_id:{{ session("id") }},
                        is_follow:isFollow
                    },
                    beforeSend: function() {
                        $('#loader').show();
                    },
                    complete: function(){
                        $('#loader').hide();
                    },
                    success : function(response){
                        console.log(response);
                        if(!response.err){
                            document.location.reload(true);
                        }
                    },
                });
            }

            function signOut(){
                $.ajax({
                    url: '/nightblog/logout',
                    type: 'POST', 
                    data: {
                    },
                    beforeSend: function() {
                        $('#loader').show();
                    },
                    complete: function(){
                        $('#loader').hide();
                    },
                    success : function(response){
                        console.log(response);
                        if(!response.err){
                            location.href='/nightblog/landing?logout=true';
                        }
                    },
                });

            }

            function bookMark(id,is_bookmark){
                console.log(id+"---"+is_bookmark);
                var datum = {
                    post_id:id,
                    is_bookmark:is_bookmark
                    };
                $.ajax({
                    url: '/nightblog/post/set_bookmark',
                    type: 'POST', 
                    data: datum,
                    success : function(response){
                    console.log(response);
                    if(response.err){
                        console.log(response.respMessage);
                    }else{
                        document.location.reload(true);
                    }
                    },
                });
            }

            function loveUnlove(id,isLove){
                console.log(id+"---"+isLove);
                var datum = {
                    id:id,
                    isLove:isLove
                    };
                $.ajax({
                    url: '/nightblog/post/love_unlove',
                    type: 'POST', 
                    data: datum,
                    success : function(response){
                    console.log(response);
                    document.location.reload(true);
                    },
                });
            }
            
    </script>
@endsection
    
    
