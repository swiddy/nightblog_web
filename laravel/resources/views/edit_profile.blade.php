@extends("templates/template")
@section("content")
<!-- Bootstrap core CSS -->
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.10.1/css/mdb.min.css" rel="stylesheet">
<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.10.1/js/mdb.min.js"></script>
<script src="{{ asset('node_modules/jquery/dist/jquery.min.js')}}"></script> 

  <!-- Blog Entries Column -->
  <div class="col-md-2"></div>
  <div class="col-md-8">
      <br/>
      <div class="card mb-4">
          <div class="card-body">
            <center>
                <img id="image_profile" src="{{$user->pic}}" style="width:200px;height:200px;border-radius:50%"/><br>
                <button style="margin-top:-150px;color:white" class="btn btn-success" id="upload"><i class="fa fa-spinner fa-spin" id="loader2"></i><i class="fa fa-upload" aria-hidden="true"></i></button>
                <input type="file" name="foto" id="foto" style="display:none"/>
            </center>
                <form id="simpan">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend2"><i class="fa fa-user-circle-o" aria-hidden="true"></i></span>
                        </div>
                        <input type="text" style="margin-left:10px" class="form-control" placeholder="Name" name="nama" value="{{$user->name}}" aria-describedby="inputGroupPrepend2" >
                    </div>
                    <br>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend2"><i class="fa fa-suitcase" aria-hidden="true"></i></span>
                        </div>
                        <input type="text" style="margin-left:10px" class="form-control"  placeholder="Profesi" value="{{$user->profesi}}" name="profesi" aria-describedby="inputGroupPrepend2" >
                    </div>
                    <br>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend2"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                        </div>
                        <input type="text" style="margin-left:10px" class="form-control" placeholder="Kota" value="{{$user->lokasi}}" name="kota" aria-describedby="inputGroupPrepend2" >
                    </div>
                    <br>
                    <div class="input-group"> 
                      <span class="input-group-text" id="inputGroupPrepend2"><i class="fa fa-lock" aria-hidden="true"></i></span>
                      <div class="switch" style="margin-left:10px">
                        <label>
                        <input id="myCheck" type="checkbox" @if($user->islocked==1) checked @endif >
                          <span class="lever"></span>
                        </label>
                      </div>
                    </div>
                  <br>
                    <button type="submit" class="btn btn-primary" style="color:white"><i class="fa fa-spinner fa-spin" id="loader"></i> Simpan</button>
                </form>
          </div>
      </div>
  </div>
  <div class="col-md-2"></div>
   <!-- Modal -->
<div class="modal fade" id="loaderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm">
      <div class="modal-content">
        <div class="modal-body" id="content_loader">
            <center>Please wait...<br><i class="fa fa-spinner fa-4x fa-spin" id="loader1"></i></center>
        </div>
      </div>
    </div>
  </div>
<script>
  var cover = "";
  var checkBox = document.getElementById("myCheck");
  

  $('#loader2').hide();
$("#upload").click(function(e){
  $("input[id='foto']").click();
  $('#foto').change(function(){
    var data = new FormData();
    data.append('image', $('#foto').prop('files')[0]);
      $.ajax({
        xhr: function() {
            var xhr = new window.XMLHttpRequest();

            xhr.upload.addEventListener("progress", function(evt) {
              if (evt.lengthComputable) {
                  percentComplete = evt.loaded / evt.total;
                  percentComplete = parseInt(percentComplete * 100);
                  console.log(percentComplete);
                  showLoader();
                  $("#content_loader").html('<i class="fa fa-spinner fa-4x fa-spin" id="loader1"></i> Uploading..('+percentComplete+'%)')
              }
            }, false);

            return xhr;
          },
          url: "/nightblog/post/upload/image",
          type: "post",
          dataType: 'json',
          processData: false,
          contentType: false,
          data: data,
          beforeSend: function(){
            showLoader();
          },
          success: function(data){
            $('#loader2').hide();
            hideLoader();
            if(cover === ""){
              cover = "http://res.cloudinary.com/nightblog/image/upload/w_300,h_300,c_fill/"+data.image;
            }
            console.log(data);
            $("#image_profile").attr('src', "http://res.cloudinary.com/nightblog/image/upload/w_300,h_300,c_fill/"+data.image);
          },
          error: function(xhr, status, error) {
            console.log(error);
          }
      });
  });
});


  $('#loader').hide();
  $("#simpan").on('submit',function(e){
    var nama = $("input[name=nama]").val();
    var profesi = $("input[name=profesi]").val();
    var kota = $("input[name=kota]").val();
    var is_checked = checkBox.checked ? 1 : 0;

  var xxx = {
            nama:nama,
            profesi:profesi,
            kota:kota,
            foto:cover,
            locked:is_checked
          };
          alert(cover);
    e.preventDefault();
    $.ajax({
          url: '/nightblog/profile/update',
          type: 'POST', 
          data: xxx,
          beforeSend: function() {
            $('#loader').show();
          },
          complete: function(){
            $('#loader').hide();
          },
          success : function(response){
            console.log(response);
            console.log(xxx);
            alert(response.respMessage);
          },
        });
  });

  function showLoader(){
    $('#loaderModal').appendTo("body").modal('show');
  }

  function hideLoader(){
    $('#loaderModal').appendTo("body").modal('hide');
  }
    
</script>

@endsection