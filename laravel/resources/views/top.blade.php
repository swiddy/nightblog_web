
    @extends('templates/template')

    @section('content')
    <style>
        .nav-tabs .nav-link {
            color: aliceblue;
        }
        .nav-tabs .nav-link.active {
            color: aliceblue;
        }

        .card-header {
          padding: .75rem 1.25rem;
          margin-bottom: 0;
          background-color: #fff;
          border-bottom: 0px solid rgba(0,0,0,.12);
      }

      .pl-3, .px-3 {
            padding-left: 10px!important;
        }
        .pb-3, .py-3 {
            padding-bottom: 0rem!important;
        }
        .pr-3, .px-3 {
            padding-right: 10px!important;
        }
        .pt-3, .py-3 {
            padding-top: 1rem!important;
        }

        .container {
            width: 100%;
            padding-right: 0px;
            padding-left: 0px;
            margin-right: auto;
            margin-left: auto;
        }

        .two-lines {
          -webkit-line-clamp: 2;
          display: -webkit-box;
          -webkit-box-orient: vertical;
          white-space: normal;
          overflow: hidden;
      }
    </style>
    <!-- Blog Entries Column -->
    <div class="col-md-2"></div>
        <div class="col-md-8">
            <nav>
                <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist" style="background:#263238">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Post</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">People</a>
                </div>
            </nav>
            <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        @if (!empty($res))
                        @foreach ($res as $res)
                            <!-- Blog Post -->
                            <div class="card mb-4">
                            <div class="card-header">
                                <span style="float:left">
                                @if($res->pic != "")
                                  <img onError="this.onerror=null;this.src='https://demo.sukowidodo.com/nightblog/public/my_template/image/logo.jpeg';" onclick="location.href='/nightblog/profile/{{$res->UserId}}/<?php echo session('id');?>';" src="{{ str_replace("http://res.cloudinary.com/nightblog/image/upload/w_300,h_300","http://res.cloudinary.com/nightblog/image/upload/w_40,h_40",$res->pic)}}" class="img img-circle" style="border-radius:50%"/> 
                                @else
                                    <img src="{{ asset('my_template/image/logo.jpeg')}}" style="width:40px;height:40px;border-radius:50%" class="img img-circle" onclick="location.href='/nightblog/profile/{{$res->UserId}}/<?php echo session('id');?>';"/>
                                @endif
                                </span>
                                <span style="float:left;margin-top:10px;margin-left:20px;color:#3297fc" ><b>{{$res->UserName}}</b></span>
                                @if($res->IsBookmark==1)
                                <span style="float:right;margin-top:10px;margin-left:10px;" onclick="bookMark({{$res->PostId}},true)"><i class="fa fa-bookmark fa-2x"></i></span>
                                @else
                                <span style="float:right;margin-top:10px;margin-left:10px;" onclick="bookMark({{$res->PostId}},false)"><i class="fa fa-bookmark-o fa-2x"></i></span>
                                @endif
                            </div>
                              
                            <div class="card-body" onclick="location.href='/nightblog/post/detail/{{$res->PostId}}';">
                              <div class="row">
                                <div class="col-8">
                                  <h2 class="card-title two-lines" style="margin-bottom:1px">{{ $res->Title }}</h2>
                                  <p class="card-text" style="margin-bottom:1px"><b>{{ "(".$res->CategoryName.")" }}</b></p>
                                  <p class="card-text" ><?php echo string(strip_tags(htmlspecialchars_decode($res->Content), '<p>'))->tease(50); ?></p>
                                </div>
                                <div class="col-4">
                                    @if ($res->Cover)
                                    <img onclick="location.href='/nightblog/post/detail/{{$res->PostId}}';" class="card-img-top thumb-post img-fluid" src="{{"http://res.cloudinary.com/nightblog/image/upload/w_300,h_300,c_fill/".$res->Cover}}">
                                    {{-- @else
                                    <img onclick="location.href='/nightblog/post/detail/{{$res->PostId}}';" class="card-img-top thumb-post img-fluid" src="http://placehold.it/750x300" styles="height:100px;"> --}}
                                    @endif
                                </div>
                              </div>
                              </div>
                            <div class="card-footer">
                                @if($res->IsLove == 1)
                                  <i class="fa fa-heart text-danger" aria-hidden="true" onclick="loveUnlove({{$res->PostId}},true)"></i> 
                                @else
                                  <i class="fa fa-heart-o" aria-hidden="true" onclick="loveUnlove({{$res->PostId}},false)"></i>
                                @endif
                                {{$res->LoveCount}} <img src="{{asset('my_template/image/ic_comment_custom.png')}}" style="width:14px;height:14px"/> {{$res->CommentCount}}
                                <?php
                                $format = 'Y-m-d H:i:s';
                                $date = DateTime::createFromFormat($format, $res->PublishDate);
                                $date = $date->format('j M Y H:i');
                              ?>
                                <span style="float:right;margin-top:10px;margin-left:10px;"><i class="fa fa-clock-o" aria-hidden="true"></i> {{$date}}</span>
                            </div>
                            </div>
                        @endforeach
                        @else
                            <br>
                            <!-- Blog Post -->
                            <div class="card mb-4">
                            <div class="card-body">
                                <p class="card-text">there is no data</p>
                            </div>
                            </div>
                        @endif
                </div>
                <div class="tab-pane fade show" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                  <table>
                    @foreach ($res2 as $res2)
                    <?php
                    if(empty($res2->pic)){
                        if ($res2->password == "3667b4d8fffc4a51b5fa71ff226e4181") {
                            $res2->pic = "https://graph.facebook.com/".$res2->username."/picture?type=large";
                        } else if ($res2->password == "b49179f14eb706bfeb6a18bb771b493f") {
                            $res2->pic = $res2->username;
                        }else{
                            $res2->pic = "https://demo.sukowidodo.com/nightblog/public/my_template/image/logo.jpeg";
                        }
                    }else if(substr($res2->pic,0,4)=="http"){
                        $res2->pic = $res2->pic;
                    }   
                    ?>
                    <tr onclick="location.href='/nightblog/profile/{{$res2->UserId}}/<?php echo session('id');?>';">
                        <td>
                            <img onError="this.onerror=null;this.src='https://demo.sukowidodo.com/nightblog/public/my_template/image/logo.jpeg';" src="{{$res2->pic}}" style="width:40px;height:40px;border-radius:50%;margin:5px;" onclick="location.href='/nightblog/profile/{{$res2->UserId}}/<?php echo session('id');?>';"/>
                        </td>
                        <td>
                            <b>{{ $res2->name }}</b><br>
                            {{ $res2->lokasi }}
                        </td>
                    <tr>
                    @endforeach
                      </table>
                </div>
            </div>
        </div>
    <div class="col-md-2"></div>

      <script>
        function loveUnlove(id,isLove){
          console.log(id+"---"+isLove);
          var datum = {
              id:id,
              isLove:isLove
            };
          $.ajax({
            url: '/nightblog/post/love_unlove',
            type: 'POST', 
            data: datum,
            success : function(response){
              console.log(response);
              document.location.reload(true);
            },
          });
        }

        function bookMark(id,is_bookmark){
          console.log(id+"---"+is_bookmark);
          var datum = {
            post_id:id,
            is_bookmark:is_bookmark
            };
          $.ajax({
            url: '/nightblog/post/set_bookmark',
            type: 'POST', 
            data: datum,
            success : function(response){
              console.log(response);
              if(response.err){
                console.log(response.respMessage);
              }else{
                document.location.reload(true);
              }
            },
          });
        }

        
      </script>

  @endsection

