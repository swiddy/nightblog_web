<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Material Design for Bootstrap fonts and icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">

    <!-- Material Design for Bootstrap CSS -->
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">

      <!-- Bootstrap core CSS -->
    {{-- <link href={{ asset('my_template/vendor/bootstrap/css/bootstrap.min.css' )}} rel="stylesheet"> --}}
    <title>Nightblog</title>
  </head>
  <body>
    <style>
      .nav .nav-item {
          margin-right: 10px;
        }
        .float{
          position:fixed;
          bottom:40px;
          right:40px;
          background-color:#bec2c6;
          color:#FFF;
          text-align:center;
        }

        .navbar-dark .navbar-toggler {
            color: hsla(0,0%,100%,.5);
            border-color: #424242!important;
        }

        .badge-notify{
          background:#4CD964;
          position:relative;
          top: -12px;
          left: -5px;
          }

          .container {
            width: 100%;
            padding-right: 0px;
            padding-left: 0px;
            margin-right: auto;
            margin-left: auto;
        }
      </style>
    <!-- Page Content -->
    <div class="bmd-layout-container bmd-drawer-f-l bmd-drawer-overlay fixed-top" style="height:800px">
        <header class="bmd-layout-header">
          <div class="navbar navbar-dark fixed-top" style="background:#263238">
            <button class="navbar-toggler" type="button" data-toggle="drawer" data-target="#dw-s2">
              <span class="sr-only">Toggle drawer</span>
              <i class="material-icons">menu</i>
            </button>
            <span style="color:aliceblue">Nightblog</span>
            <ul class="nav justify-content-end" style="color:#FFFFFF">
                @if(isset($post))
                  @if($user->id == session("id"))
                    <li class="nav-item" onclick="showModal()">
                      <i class="fa fa-trash" aria-hidden="true"></i>
                    </li>
                  @endif
                  <li class="nav-item" onclick="share()">
                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                  </li>
                @else
                <li class="nav-item active" onclick="window.location.href='/nightblog/search'">
                    <i class="fa fa-search" aria-hidden="true"></i>
                      <span class="sr-only">(current)</span>
                  </li> 
                  <li class="nav-item" onclick="window.location.href='/nightblog/top'">
                    <i class="fa fa-star" aria-hidden="true"></i>
                  </li>
                  <li class="nav-item" onclick="window.location.href='/nightblog/notifikasi'">
                    <i class="fa fa-bell" aria-hidden="true"></i>
                    <span class="badge badge-notify" style="font-size:10px;hidden"></span>
                  </li>
                  @endif
            </ul>
          </div>
        </header>
        <div id="dw-s2" class="bmd-layout-drawer" style="background-position: center; background:url({{ asset('my_template/image/drawer.jpeg')}})">
          <header>
              <img onError="this.onerror=null;this.src='https://demo.sukowidodo.com/nightblog/public/my_template/image/logo.jpeg';" src="{{session('pic')}}" style="width:100px;height:100px;border-radius:50%;margin-top:15px;margin-bottom:5px"  onclick="window.location.href='/nightblog/profile/{{session('id')}}/{{session('id')}}'"/>
              <br>
              <b style="color:#FFFFFF">{{session('name')}}</b>
              <span style="color:#b1b1b3" onclick="window.location.href='/nightblog/profile/{{session('id')}}/{{session('id')}}'">See Profile</span>
              
          </header>
          
          <ul class="list-group" style="color:#FFFFFF">
            <li class="list-group-item" onclick="window.location.href='/nightblog/home'">Home</li>
            <li class="list-group-item" onclick="window.location.href='/nightblog/bookmark'">Bookmark</li>
            <li class="list-group-item" onclick="window.location.href='/nightblog/draft'">Draft</li>
            <li class="list-group-item" onclick="window.location.href='/nightblog/help'">Help</li>
            <li class="list-group-item" onclick="window.location.href='https://api.whatsapp.com/send?text=Bergabung dengan saya di Nightblog, media sosial tempat berbagi wawasan. More : https://play.google.com/store/apps/details?id=com.night.blog \n Atau \n https://nightblog.id/'">Invite Friends</li>
          </ul>
        </div>
        <main class="bmd-layout-content" style="height:100%">
            <div class="container" >
                <div class="row" style="padding-top:50px">
                    @yield('content')
                      <!-- The Modal -->
                    
                </div>
                <!-- /.row -->
              </div>
              <!-- /.container -->
              <button type="button" class="btn btn-dark bmd-btn-fab float" data-toggle="modal" data-target="#myModal">
                  <i class="material-icons">edit</i>
              </button>
      </div>
      <div class="modal fade" id="myModal">
          <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
              <div class="col-md-12" style="padding-left:0px;padding-right:0px">
                  <img src="{{ asset('my_template/image/ulasan.jpg')}}" style="height:250px;width:100%" onclick="window.location.href='/nightblog/create/ulasan'"/>
                  <b style="position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);color:#FFFFFF">Ulasan</b>
              </div>
              <div class="col-md-12" style="padding-left:0px;padding-right:0px">
                   <img src="{{ asset('my_template/image/karya_tulis.jpg')}}" style="height:250px;width:100%" onclick="window.location.href='/nightblog/create/karya_tulis'"/>
                  <b style="position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);color:#FFFFFF">Karya Tulis</b>
              </div>  
          </div>
          </div>
      </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <script src="https://use.fontawesome.com/245b0214e2.js"></script>

    <script>
      //$(".badge").hide();
      if(typeof(EventSource) !== "undefined") {
        var source = new EventSource("/nightblog/notifikasi2");
        source.addEventListener('message', event => {
                let data = JSON.parse(event.data);
                // console.log(data.length);
                if(data.length===0){
                  $(".badge").hide();
                }else{
                  $(".badge").show();
                  $(".badge").html(data.length);
                }
                
            }, false);

        source.addEventListener('error', event => {
            if (event.readyState == EventSource.CLOSED) {
                console.log('Event was closed');
                console.log(EventSource);
            }
        }, false);
      } else {
        console.log("Sorry, your browser does not support server-sent events...");
      }
      </script>

    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v5.0&appId=2280766852171099&autoLogAppEvents=1"></script>
  
    <script src="{{ asset('my_template/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('my_template/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>
    <script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
  </body>
</html>


  
 