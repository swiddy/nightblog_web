
@extends('templates/template')
@section('content')
    <div class="col-md-2"></div>
    <div class="col-md-8">
    <br>
        <div class="card mb-4">
                <div class="btn" onclick="showModal()"><i class="fa fa-trash my-float"></i> Hapus</div>
            <div class="card-body">
                @if (!empty($notif))
                    <table class="table">
                        <tbody>
                        @foreach ($notif as $notif)
                        <tr>
                            <td>
                                <img onError="this.onerror=null;this.src='https://demo.sukowidodo.com/nightblog/public/my_template/image/logo.jpeg';" src="{{$notif->pic}}" style="width:80px;height:80px;border-radius:50%" onclick="location.href='/nightblog/profile/{{$notif->id_client}}/<?php echo session('id');?>';"/>
                            </td>
                            <td>
                                <b>{{ $notif->title }}</b><br>
                                {{ $notif->content }}<br>
                                {{ $notif->create_at }}
                            </td>
                        <tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>
                               Tidak ada data
                            </td>
                        <tr>
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-2"></div>

    <!-- Modal -->
    <div class="modal fade" id="modalWarning" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Warning</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <h5>Apakah anda yakin ingin menghapus?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" onclick="truncate()">Hapus</button>
            </div>
          </div>
        </div>
      </div>

    <script>
        function truncate(){
                $.ajax({
                url: '/nightblog/notifikasi/truncate',
                type: 'POST', 
                data: "",
                success : function(response){
                    $('#modalWarning').modal('hide');
                    //alert(response.respMessage);
                    document.location.reload(true);
                },
            });
        }

        function showModal(){
            $('#modalWarning').appendTo("body").modal('show');
        }
    </script>
@endsection
    
   