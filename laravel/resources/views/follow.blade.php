@extends("templates/template")
@section("content")
  <!-- Blog Entries Column -->
  <div class="col-md-2"></div>
  <div class="col-md-8">
      <br/>
      <div class="card mb-4">
          <div class="card-body" >
              <table>
            @foreach ($data as $data)
            <tr onclick="location.href='/nightblog/profile/{{$data->UserId}}/<?php echo session('id');?>';">
                <td>
                    <img src="{{$data->pic}}" style="width:40px;height:40px;border-radius:50%;margin:5px;" onclick="location.href='/nightblog/profile/{{$data->UserId}}/<?php echo session('id');?>';"/>
                </td>
                <td>
                    <b>{{ $data->name }}</b><br>
                    {{ $data->lokasi }}
                </td>
            <tr>
            @endforeach
              </table>
          </div>
      </div>
  </div>
  <div class="col-md-2"></div>
@endsection