
@extends('templates/template')
@section('content')
<script src="{{ asset('node_modules/jquery/dist/jquery.min.js')}}"></script> 
      <!-- Blog Entries Column -->
      <style>
      .card-header-x {
          padding: .75rem 1.25rem;
          margin-bottom: 0;
          background-color: #fff;
          border-bottom: 0px solid rgba(0,0,0,.12);
      }
      .container {
            width: 100%;
            padding-right: 0px;
            padding-left: 0px;
            margin-right: auto;
            margin-left: auto;
        }
      </style>
      <div class="col-md-2"></div>
      <div class="col-md-8">
        <br>
        <div class="row">
          {{-- @if($user->id == session("id"))
              <div class="btn btn-outline-primary" style="margin-left:15px;float:right" onclick="showModal()"><i class="fa fa-trash my-float"></i> Hapus</div>
          @endif
          <div class="btn btn-outline-primary" style="margin-left:15px" onclick="share()"><i class="fa fa-share my-float"></i> Bagikan</div> --}}
        </div>
        <!-- Blog Post -->
        <div class="card mb-4">
          
          <div class="card-header-x">
             <span style="float:left">
              @if($user->pic != "")
                <img src="{{ str_replace("http://res.cloudinary.com/nightblog/image/upload/w_300,h_300","http://res.cloudinary.com/nightblog/image/upload/w_40,h_40",$user->pic)}}" class="img img-circle" style="border-radius:50%" alt="sdss"/> 
              @else
                <img src="{{ asset('my_template/image/logo.jpeg')}}" style="width:40px;height:40px;border-radius:50%" class="img img-circle"/>
              @endif
              </span>
              <span style="float:left;margin-top:10px;margin-left:10px;"><b>{{$user->name}}</b></span>
              @if ($is_bookmark > 0)
                <span style="float:right;margin-top:10px;margin-left:10px;" onclick="bookMark({{$post->id}},true)"><i class="fa fa-bookmark fa-2x"></i></span>
              @else
                <span style="float:right;margin-top:10px;margin-left:10px;" onclick="bookMark({{$post->id}},false)"><i class="fa fa-bookmark-o fa-2x"></i></span>
              @endif
              
          </div>
          @if($post->category_id==1)
            @if ($post->cover)
              <img class="card-img-top thumb-post img-fluid" src="{{"http://res.cloudinary.com/nightblog/image/upload/w_750,h_750,c_fill/".$post->cover}}" styles="height:100px;">
            {{-- @else
              <img class="card-img-top thumb-post img-fluid" src="http://placehold.it/750x300" styles="height:100px;"> --}}
            @endif
          @endif
          
          <?php 
          if(substr($post->background,0,4) === "http"){
            $bg = "style=background:url('".$post->background."');background-size:100%";
          }else if(substr($post->background,0,1) === "#"){
            $bg = "style=background:".$post->background;
          }else{
            $bg = "";
          }?>

          <div class="card-body" {{$bg}}>
            <h2 class="card-title" style="margin-bottom:1px"><b>{{ $post->title }}</b></h2>
            @if($post->category_id==1)
            <b class="card-title" style="margin-bottom:1px"><i>(Ulasan)</i></b>
            @elseif($post->category_id==3)
            <b class="card-title" style="margin-bottom:1px"><i>(Karya Tulis)</i></b>
            @endif
            <?php
              $format = 'Y-m-d H:i:s';
              $date = DateTime::createFromFormat($format, $post->publish_date);
              $datex = $date->format('j M Y H:i');
            ?>
            <br/>
            <i class="fa fa-clock-o" aria-hidden="true"></i> {{ $datex }}
            <br/>
            <p class="card-text">
              <?php echo $post->content; ?>
            </p>
                  <p>
                  @if(empty($post->attachment1) == false 
                      || empty($post->attachment2) == false 
                          || empty($post->attachment3) == false)
                    <b>Attachment</b><br>
                  @endif
                  @if(!empty($post->attachment1))
                  <a href="{{ $post->attachment1 }}">{{ $post->attachment1 }}</a><br/>
                  @endif
                  @if(!empty($post->attachment2))
                  <a href="{{ $post->attachment2 }}">{{ $post->attachment2 }}</a><br/>
                  @endif
                  @if(!empty($post->attachment3))
                  <a href="{{ $post->attachment3 }}">{{ $post->attachment3 }}</a><br/>
                  @endif
                  </p>
          </div>
          <div class="card-footer ">
            
            @if($isLove)
              <i class="fa fa-heart text-danger" style="height:14px" aria-hidden="true" onclick="loveUnlove({{$post->id}},true)"></i> 
            @else
              <i class="fa fa-heart-o" aria-hidden="true" style="height:14px" onclick="loveUnlove({{$post->id}},false)"></i>
            @endif
             {{count($like)}}
            <img src="{{asset('my_template/image/ic_comment_custom.png')}}" style="width:14px;height:14px"/> {{ count($comment) }}
          </div>
        </div>
      </div>
      <div class="col-md-2"></div>

      <div class="col-md-2"></div>
      <div class="col-md-8">
        
        <!-- Blog Post -->
        <div class="card mb-4" >
            <div class="card-header">
            <h5>
              {{-- <i class="fa fa-comment text-success" aria-hidden="true" ></i>  --}}
              {{ count($comment) }} COMMENT </h5>
             </div>
            @foreach($comment as $comment)
              <div class="card-body">
                <span style="float:left">
                  @if($comment->pic != "")
                    <img onError="this.onerror=null;this.src='https://demo.sukowidodo.com/nightblog/public/my_template/image/logo.jpeg';" src="{{ str_replace("http://res.cloudinary.com/nightblog/image/upload/w_300,h_300","http://res.cloudinary.com/nightblog/image/upload/w_50,h_50",$comment->pic)}}" class="img img-circle" style="border-radius:5%" alt="sdss"/> 
                  @else
                    <img src="{{ asset('my_template/image/logo.jpeg')}}" style="width:40px;height:40px"/>
                  @endif
                </span>
                <span style="float:left;margin-top:0px;margin-left:10px;">
                  <b>{{$comment->name}}</b></br>
                  {{$comment->comment}}<br/>
                  <span style="font-size:12px">{{$comment->publish_date}}</span>
                </span>
              </div>
              <hr>
            @endforeach
            <div class="card-footer ">
                <div class="form-group">
                    <textarea class="form-control" rows="5" name="comment" ></textarea>
                </div>
                <button class="btn btn-primary" id="comment" ><i class="fa fa-spinner fa-spin" id="loader"></i>Submit</button>
            </div>
        </div>
       

      </div>
      <div class="col-md-2"></div>

      <!-- Modal -->
    <div class="modal fade" id="modalWarning" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Warning</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <h5>Apakah anda yakin ingin menghapus?</h5>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" onclick="hapus()">Hapus</button>
            </div>
          </div>
        </div>
      </div>

      <!-- Modal -->
    <div class="modal fade" id="modalShare" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Share</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <center>
              <div class="btn btn-outline-primary" style="margin-left:15px" onclick="window.location.href='https://api.whatsapp.com/send?text= [{{ $post->title }}]: https://play.google.com/store/apps/details?id=com.night.blog \n\nThanks'"><i class="fa fa-whatsapp my-float"></i> Whatsapp</div>
              <div class="btn btn-outline-primary" style="margin-left:5px" onclick="window.location.href='https://twitter.com/intent/tweet?text=[{{ $post->title }}]: https://play.google.com/store/apps/details?id=com.night.blog \n\nThanks'" data-size="large"><i class="fa fa-twitter my-float"></i> Twitter</div>
              <div class="btn btn-outline-success" style="margin-left:5px" data-href="https://play.google.com/store/apps/details?id=com.night.blog" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://play.google.com/store/apps/details?id=com.night.blog;src=sdkpreparse" class="fb-xfbml-parse-ignore"><i class="fa fa-facebook my-float"></i> Facebook</a></div>
            </center>
            </div>
        </div>
      </div>
    </div>

    <script>
      function showModal(){
            $('#modalWarning').appendTo("body").modal('show');
        }
      $('#loader').hide();
      $("#comment").click(function(e){
        $('#loader').hide();
        e.preventDefault();
        comment = $("textarea[name=comment]").val();
        $.ajax({
          url: '/nightblog/post/add/comment',
          type: 'POST', 
          data: {
            post_id:"{{$post->id}}",
            user_id: "{{session('id')}}",
            comment: comment
          },
          beforeSend: function() {
            $('#loader').show();
          },
          complete: function(){
            $('#loader').hide();
          },
          success : function(response){
            console.log("response",response);
            if(!response.err){
              document.location.reload(true);
            }
          },
          onerror:function(err){
            console.log(err);
          }
        });
      });
      
    </script>
    <script>
        function loveUnlove(id,isLove){
          console.log(id+"---"+isLove);
          var datum = {
              id:id,
              isLove:isLove
            };
          $.ajax({
            url: '/nightblog/post/love_unlove',
            type: 'POST', 
            data: datum,
            success : function(response){
              //console.log(response);
              document.location.reload(true);
            },
          });
        }

        function bookMark(id,is_bookmark){
          console.log(id+"---"+is_bookmark);
          var datum = {
              post_id:id,
              is_bookmark:is_bookmark
            };
          $.ajax({
            url: '/nightblog/post/set_bookmark',
            type: 'POST', 
            data: datum,
            success : function(response){
              console.log(response);
              if(!response.err){
                document.location.reload(true);
              }else{
                alert(response.respMessage)
              }
              
            },
          });
        }

        function hapus(){
          $.ajax({
            url: '/nightblog/post/delete',
            type: 'POST', 
            data: {id:{{$post->id}}},
            success : function(response){
              console.log(response);
              if(!response.err){
                window.location.replace("/nightblog/home");
              }else{
                alert(response.respMessage)
              }
              
            },
          });
        }


        function share(){
            let shareData = {
            title: '',
            text: '[{{$post->title}}] @if($post->category_id==1)[Ulasan] @else [Karya tulis] @endif {{ $date->format("Y-m-d")}} by {{$user->name}}. More : https://play.google.com/store/apps/details?id=com.night.blog \n Atau \n',
            url: 'https://nightblog.id'
          }

          if (navigator.share) {
            navigator.share(shareData)
              .then(() => console.log('Successful share'))
              .catch((error) => console.log('Error sharing', error));
          }else{
            $('#modalShare').appendTo("body").modal('show');
          }
        }
      </script>
@endsection
    
