@extends("templates/template")
@section("content")
  <!-- Blog Entries Column -->
  <div class="col-md-2"></div>
  <div class="col-md-8">
      <br/>
      <div class="card mb-4">
          <div class="card-body">
            <b>Apa itu Nightblog?</b><p>Nightblog adalah jenis media jejaring sosial.</p><p> Nightblog hadir dengan konsep ruang bacaan bagi pecinta karya tulis maupun bagi seseorang yang sedang mencari inspirasi. Dengan menjadikan tulisan berupa blog, article, karya tulis menjadi menu utama, Nightblog memunculkan trend membaca ke dunia digital.</p><p> Memberikan kesempatan bagi penulis-penulis amatir sampai profesional untuk memamerkan karyanya dalam media jejaring sosial yang lebih luas, memberikan wadah yang menarik untuk meningkatkan budaya literasi Indonesia.</p>
            <b>Kenapa saya menggunakan Nightblog?</b><p>Karena Nightblog adalah wadah untuk para pengguna membagikan wawasan dan karya tulis kepada teman dan para pengguna lain, dengan fitur – fitur yang memudahkan pengguna untuk membuat tulisan mereka dan mendiskusikannya sehingga bertambah wawasan para pengguna.</p>
            <b>Kenapa membagikan wawasan itu menjadi penting?</b><p>Karena dengan berbagi wawasan dan mendiskusikannya maka akan bertambah wawasan-wawasan baru, seperti ide awal penemu mesin cetak buku Gutenberg yang meyakini pentingya menduplikasi dan menyebarkan  wawasan/knowledge untuk dunia yang lebih baik.</p>
            <b>Bagaimana saya mengundang teman untuk bergabung di Nightblog?</b><p>Kamu dapat mengundang teman bergabung di Nightblog dengan fitur Invite Friends, yang akan menghubungkan dengan kontak di ponsel. Bisa satu per satu kontak di undang atau undang ke banyak teman sekaligus lewat Whatsapp dengan memilih \"UNDANG KE BANYAK\ sehingga kamu bisa tahu wawasan-wawasan yang dibagikan oleh temanmu.</p>
            <b>Apa itu fitur ulasan?</b><p>Bagikan wawasan/knowledge  yang pengguna dapatkan dari buku yang telah di baca, dengan memfoto sampul buku dan berikan ulasannya agar orang lain mendapatkan manfaat dari wawasan/knowledge dari buku yang pengguna baca.</p>
            <b>Saya mendapatkan wawasan/pengetahuan tidak hanya dari buku</b><p>Pada aplikasi Nightblog versi selanjutnya akan ada fitur Ulasan Film & Ulasan Tautan.</p>
            <b>Apa itu fitur Karya Tulis?</b><p>Pengguna dapat menuliskan karya tulis seperti puisi, tutorial, karya ilmiah, blog dll, sehingga karya-karya mereka bisa mendapat perhatian dari para pembaca yang ada di Nightblog.</p><p> Pengguna juga dapat menggunakan Word/PDF untuk upload dokumen, dengan fitur dan tampilan yang memudahkan pengguna sehingga pengguna dapat menggunakannya dengan mudah tanpa harus mempunyai pengalaman dengan PC/Laptop.</p>
            <b>Apa itu Bookmark?</b><p>Bookmarks adalah penanda untuk menandakan postingan mana yang mungkin suatu saat akan pengguna lihat kembali.</p>
            <b>Apa itu Draft?</b><p>Ini yang tidak dimiliki platform media sosial lain, pengguna dapat menyimpan rangkaian tulisannya untuk dilanjutkan di kemudian hari tanpa harus mengunggahnya terlebih dahulu.</p>
            <b>Apakah saya dapat login dengan Facebook atau Gmail?</b><p>Ya tentu, hanya dengan men tap Masuk (pada icon Facebook) atau Continue with Google pada halaman login.</p>
            <b>Apa itu Feed</b><p>Postingan - postingan yang memiliki likes dan comment yang tinggi akan dipamerkan pada halaman Feed secara global.</p>
            <b>Contact</b><p>Untuk informasi lebih lanjut bisa mengirimkan email ke alamat : Nightblog.info@gmail.com<p>
          </div>
      </div>
  </div>
  <div class="col-md-2"></div>
@endsection