<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="google-signin-client_id" content="513377774470-kni3oo5bm4oaq9994b9jbaqmjfng71oa.apps.googleusercontent.com">
<script src="{{ asset('node_modules/jquery/dist/jquery.min.js')}}"></script> 

<title>Nightblog</title>

<!-- Bootstrap core CSS -->
<link href={{ asset('my_template/vendor/bootstrap/css/bootstrap.min.css' )}} rel="stylesheet">


<script src="https://use.fontawesome.com/245b0214e2.js"></script>

<!-- Custom styles for this template -->
<link href={{ asset('my_template/css/blog-home.css' )}} rel="stylesheet">
<script src="https://apis.google.com/js/platform.js" async defer></script>
<style>
    body {
        background-color: #fdfdfd;
        background-image: url("data:image/svg+xml,%3Csvg width='40' height='40' viewBox='0 0 40 40' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M0 0h20v20H0V0zm10 17a7 7 0 1 0 0-14 7 7 0 0 0 0 14zm20 0a7 7 0 1 0 0-14 7 7 0 0 0 0 14zM10 37a7 7 0 1 0 0-14 7 7 0 0 0 0 14zm10-17h20v20H20V20zm10 17a7 7 0 1 0 0-14 7 7 0 0 0 0 14z' fill='%23e8e8e8' fill-opacity='0.4' fill-rule='evenodd'/%3E%3C/svg%3E");
    }
    
</style>
</head>

<body>
  <!-- Page Content -->
  <div class="container" >
    <div class="row">
      <!-- Blog Entries Column -->
      <div class="col-md-3"></div>
      <div class="col-md-6" style="margin-top:-60px">
        <!-- Blog Post -->
         <div class="card mb-4">
          <img class="card-img-top thumb-post img-fluid" src="{{ asset('my_template/image/nb.png')}}" styles="height:100px;">
          <div class="card-body">
            <div class="card" style="margin-top:-50px">
                <div class="card-body">
                    <form>
                        <div class="form-group">
                            <center>
                            
                              <fb:login-button data-width="260px" data-size="large" data-button-type="continue_with" scope="public_profile,email" onlogin="checkLoginState();">
                              </fb:login-button>
                              
                              <div id="status">

                            <div style="margin-top:10px" class="g-signin2" onclick="renderButton()" id="my-signin2"></div>
                            <div class="btn btn-primary" style="width:260px;margin-top:10px" onclick="location.href='/nightblog/register';">DAFTAR</div>
                            <p>Sudah punya akun? <a href="/nightblog/login">Login</a>
                            </center>
                        </div>
                    </form>
                </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3"></div>
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->

  <!-- Bootstrap core JavaScript -->
  <script src="{{ asset('my_template/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{ asset('my_template/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>

  <script type="text/javascript">
    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });
    $(".btn-submit").click(function(e){
        e.preventDefault();
        var username = $("input[name=username]").val();
        var password = $("input[name=password]").val();
        $.ajax({
           type:'POST',
           url:'/nightblog/do_login',
           data: {
             username:username, 
             password:password
            },
           success:function(data){
              console.log(data);
              if(data.err==false){
                  window.location.replace("/nightblog/home");
              }else{
                  alert(data.errMsg);
              }
           }
        });
      });

      function onSuccess(googleUser) {
        googleUser.disconnect();
        var profile = googleUser.getBasicProfile();
        
            console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
            console.log('Name: ' + profile.getName());
            console.log('Image URL: ' + profile.getImageUrl());
            console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
            login({
                id:profile.getId(),
                name:profile.getName(),
                pic:profile.getImageUrl(),
                email:profile.getEmail(),
                type:"gg"
              });
              

      }
      function onFailure(error) {
        console.log(error);
      }
      function renderButton() {
        gapi.signin2.render('my-signin2', {
          'scope': 'profile email',
          'width': 260,
          'height': 50,
          'longtitle': true,
          'theme': 'dark',
          'onsuccess': onSuccess,
          'onfailure': onFailure
        });
    }

    function statusChangeCallback(response) {  // Called with the results from FB.getLoginStatus().
      FB.api('/me?fields=name,email', function(response) {
        login({
                id:response.id,
                name:response.name,
                pic:"https://graph.facebook.com/" + response.id + "/picture?type=large",
                email:response.email,
                type:"fb"
              });
      });
    
    }


  function checkLoginState() {               // Called when a person is finished with the Login Button.
    FB.getLoginStatus(function(response) {   // See the onlogin handler
      statusChangeCallback(response);
    });
  }


  window.fbAsyncInit = function() {
    FB.init({
      appId      : '2210148992573790',
      cookie     : true,                     // Enable cookies to allow the server to access the session.
      xfbml      : true,                     // Parse social plugins on this webpage.
      version    : 'v4.0'           // Use this Graph API version for this call.
    });

    @if($islogout == false)
    FB.getLoginStatus(function(response) {   // Called after the JS SDK has been initialized.
      statusChangeCallback(response);        // Returns the login status.
    });
    @else
    FB.logout(function(response) {}
      // user is now logged out
    );
    @endif
  };

  
  (function(d, s, id) {                      // Load the SDK asynchronously
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

function login(datum){
  console.log(datum);
  post('/nightblog/login/social',datum);
  // $.ajax({
  //          type:'POST',
  //          url:'/nightblog/login/social',
  //          data: datum,
  //          beforeSend: function(){
  //           console.log("tes");
  //         },
  //          success:function(data){
  //             console.log(data);
  //             if(data.err==false){
  //               location.href='/nightblog/home';
  //             }
  //          },
  //          error: function(xhr, status, error) {
  //         //var err = eval("(" + xhr.responseText + ")");
  //         //alert(err.Message);
  //           console.log(error);
  //         }
  //       });
}

function post(path, params, method='post') {

// The rest of this code assumes you are not using a library.
// It can be made less wordy if you use one.
const form = document.createElement('form');
form.method = method;
form.action = path;

for (const key in params) {
  if (params.hasOwnProperty(key)) {
    const hiddenField = document.createElement('input');
    hiddenField.type = 'hidden';
    hiddenField.name = key;
    hiddenField.value = params[key];

    form.appendChild(hiddenField);
  }
}

document.body.appendChild(form);
form.submit();
}
</script>

</body>

</html>
