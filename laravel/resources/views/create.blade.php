@extends("templates/template")
@section("content")
<style 
<style>
    .tox-statusbar__branding{
        display:none;
    }

    .colorPickSelector {
      border-radius: 5px;
      width: 36px;
      height: 36px;
      cursor: pointer;
      -webkit-transition: all linear .2s;
      -moz-transition: all linear .2s;
      -ms-transition: all linear .2s;
      -o-transition: all linear .2s;
      transition: all linear .2s;
    }

.colorPickSelector:hover { transform: scale(1.1); }

</style>

<script src="{{ asset('node_modules/jquery/dist/jquery.min.js')}}"></script> 
<script src="{{ asset('node_modules/tinymce/tinymce.min.js')}}"></script> 
<script src="{{ asset('node_modules/lodash/lodash.js')}}" type="text/javascript"></script>
<script src="{{ asset('node_modules/cloudinary-core/cloudinary-core.js')}}" type="text/javascript"></script>
<script src="{{ asset('colorpicker/src/colorPick.min.js')}}"></script> 
<link rel="stylesheet" href="{{ asset('colorpicker/src/colorPick.min.css')}}">
<script src="{{ asset('my_template/js/jquery-form.js')}}"></script>

<script type="text/javascript">
  // $.cloudinary.config({"api_key":"763884272672151","cloud_name":"nightblog"});
  var percentComplete = 0;
    var cover = "";
    var background="";
    var gambar = [];
    
</script> 

@if($posting->category_id != 1)
  <script>

    tinymce.init({
        selector: 'textarea#basic-example',
        height: 700,
        menubar: false,
        force_br_newlines : true,
        force_p_newlines : false,
        forced_root_block : '',
        plugins: [
            'advlist lists image charmap print preview anchor imagetools',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code '
        ],
        toolbar: 'undo redo formatselect bold italic underline forecolor backcolor alignleft aligncenter alignright alignjustify bullist numlist outdent indent removeformat imageButton attachButton backgroundButton',
        mobile: {
          theme: 'silver',
          plugins: [ 'advlist lists image charmap print preview anchor imagetools',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code ' ],
          toolbar: ['undo redo formatselect bold italic underline forecolor backcolor alignleft aligncenter alignright alignjustify bullist numlist outdent indent removeformat imageButton attachButton backgroundButton']
        },
        setup : function(editor){
            editor.ui.registry.addButton('imageButton', {
            text: 'Image',
                onAction: function (_) {
                  $("input[id='my_file']").click();
                  $('#my_file').bind("change",function(e){
                    e.preventDefault();
                    e.stopImmediatePropagation();
                    var data = new FormData();
                    data.append('image', $('#my_file').prop('files')[0]);
                    $.ajax({
                      xhr: function() {
                            var xhr = new window.XMLHttpRequest();

                            xhr.upload.addEventListener("progress", function(evt) {
                              if (evt.lengthComputable) {
                                  percentComplete = evt.loaded / evt.total;
                                  percentComplete = parseInt(percentComplete * 100);
                                  console.log(percentComplete);
                                  showLoader();
                                  $("#content_loader").html('<i class="fa fa-spinner fa-4x fa-spin" id="loader1"></i> Uploading..('+percentComplete+'%)')
                              }
                            }, false);

                            return xhr;
                          },
                          url: "/nightblog/post/upload/image",
                          type: "post",
                          dataType: 'json',
                          processData: false,
                          contentType: false,
                          data: data,
                          beforeSend: function(){
                            console.log("tes")
                          },
                          success: function(data){
                            console.log(data);
                            percentComplete = 0;
                            $('#loaderModal').appendTo("body").modal('hide');
                            if(cover === ""){
                              cover = data.image;
                            }
                            editor.insertContent('<img class=\'img-fluid\' style=\'width:100%\' src=\''+data.url+'\'>');
                            $('#my_file').val('');

                          }
                      });
                  });
                }
            });
            editor.ui.registry.addButton('attachButton', {
            text: 'Attach',
                onAction: function (_) {
                    //editor.insertContent('&nbsp;<strong>It\'s my button!</strong>&nbsp;');
                    showModal();
                }
            });
            editor.ui.registry.addButton('backgroundButton', {
            text: 'Background',
                onAction: function (_) {
                    showModalColor(function(color){
                      console.log("colorr",color);
                      $('iframe').contents().find('body').css('background', color);
                      $('iframe').contents().find('body').css('background-size', '100%');
                    });
                }
            });

            @if($is_draft)
            editor.on('init', () => {
                editor.setContent('<?php echo str_replace(["\n","\r"],"",$posting->content)?>');
                  @if(substr($posting->background,0,4)=="http")
                    <?php echo "background=\"".$posting->background."\";"?>
                    $('iframe').contents().find('body').css('background', 'url({{$posting->background}}');
                    $('iframe').contents().find('body').css('background-size', '100%');
                  @elseif(substr($posting->background,0,1)=="#")
                    $('iframe').contents().find('body').css('background', '{{$posting->background}}');
                    $('iframe').contents().find('body').css('background-size', '100%');
                  @endif
                  @if($posting->cover)
                      cover = "{{$posting->cover}}";
                  @endif
            });
            @endif
        },
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tiny.cloud/css/codepen.min.css'
        ]
    });

 
  </script>
@else
  <script>
    tinymce.init({
        selector: 'textarea#basic-example',
        height: 700,
        menubar: false,
        force_br_newlines : true,
        force_p_newlines : false,
        forced_root_block : '',
        setup: editor => {
          @if($is_draft)
            editor.on('init', () => {
              editor.setContent('<?php echo str_replace(["\n","\r"],"",$posting->content)?>');
            });
            //$("#pic1").attr("src", searchPic);
            $("#cover-box").html("<img class=\"img-fluid\" style=\"height:100%\" src='https://res.cloudinary.com/nightblog/image/upload/{{$posting->cover}}'/>");
            $(".fa-picture-o").hide();
            cover="{{$posting->cover}}";
          @endif
        },
        plugins: [],
        toolbar: '',
        mobile: {
          theme: 'silver'}
    });
  </script>
@endif
 
  <!-- Blog Entries Column -->
  <div class="col-md-2"></div>
  <div class="col-md-8">
    <br>
    <!-- Blog Post -->
    <div class=" mb-4">
      <form id="simpan">
          <div class="btn btn-primary draft" style="float:right;margin-bottom:10px">Save as draft</div>  
          <div class="btn btn-primary simpan" style="float:right;margin-bottom:10px;margin-right:10px">
              <i class="fa fa-spinner fa-spin" id="loader1"></i>Publish
          </div>
          <br/><br/>
        @if($posting->category_id == 1)
          <div id="cover-box" style="border-radius:20px;border-style: dashed;height:200px;text-align:center;">
            <i class="fa fa-picture-o fa-2x" style="margin-top:80px;position: relative;overflow: hidden;" aria-hidden="true">
              <input id="uploadCover" name = "uploadCover" type="file" style="position: absolute;top: 0;right: 0;margin: 0;padding: 0;font-size: 20px;cursor: pointer;opacity: 0;filter: alpha(opacity=0);"/>
            </i>
          </div><br>
        @endif
        <input type="text" class="form-control" name="title" id="exampleInputEmail1" placeholder="Title" value="@if(isset($posting->title)){{$posting->title}}@endif"><br/>
        <input type="text" class="form-control" style="display:none" name="id_kategori" value="@if(isset($posting->category_id)){{$posting->category_id}}@endif">
        <textarea id="basic-example" name="content" placeholder="Type Something"></textarea>
      </form>
        <div id="attachment"></div><br>
    </div>
  </div>
  <div class="col-md-2"></div>

    <!-- Modal -->
<div class="modal fade" id="attachModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
    <form enctype="multipart/form-data" class="attach">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Add Attachment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <input type="file" class="form-control-file" name="attachment1">
        </div>
          <div class="form-group">
            <input type="file" class="form-control-file" name="attachment2">
        </div>
          <div class="form-group">
            <input type="file" class="form-control-file" name="attachment3">
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary btn-lg" id="load" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing Order"><i class="fa fa-spinner fa-spin" id="loader"></i> Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="colorPickerDialog" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
      <form enctype="multipart/form-data" class="attach">
        <div class="modal-header">
        </div>
        <div class="modal-body">
          <div class="row">
           <div class="col-6">
              <input class="btn bgcolor" style="background:#ff0000" value="#ff0000"/>
              <input class="btn bgcolor" style="background:#0066ff" value="#0066ff">
              <input class="btn bgcolor" style="background:#ffff00" value="#ffff00">
              <input class="btn bgcolor" style="background:#66ff33" value="#66ff33">
              <input class="btn bgcolor" style="background:#993300" value="#993300"> 
              <input class="btn bgcolor" style="background:#cc66ff" value="#cc66ff">
              <input class="btn bgcolor" style="background:#ff9999" value="#ff9999">
              <input  class="btn bgcolor" style="background:#ffcc00" value="#ffcc00">
           </div>
           <div class="col-6">
             <input type="file" name="bgimage" id="bgimage"/>
           </div>
          </div>
        </div>
        </form>
      </div>
    </div>
  </div>

   <!-- Modal -->
<div class="modal fade" id="loaderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm">
      <div class="modal-content">
        <div class="modal-body" id="content_loader">
            <center>Please wait...<br><i class="fa fa-spinner fa-4x fa-spin" id="loader1"></i></center>
        </div>
      </div>
    </div>
  </div>

<input type="file" id="my_file" style="display: none;" />
<script>
var attach = [];


$("#loader").hide();
$("#loader1").hide();
$(".attach").on('submit', function(e){
  e.preventDefault();
  $.ajax({
    url: '/nightblog/post/upload/attachment',
    type    : 'POST', 
    data: new FormData(this),
    contentType: false,
    cache: false,
    processData:false,
    beforeSend: function(){
      $("#loader").show();
    },
    success : function(response){
      console.log(response);
      $('#attachModal').modal('hide');
      
      $("#attachment").append("<b>Attachment</b><br>");
      for(i=0;i<response.length;i++){
        $("#attachment").append("<a href='"+response[i]+"'>"+(i+1)+". "+response[i]+"</a><br>");
        attach[i] = response[i];
      }
    },
  });
});

$(".simpan").click(function(){
  title = $("input[name=title]").val();
  content = tinymce.activeEditor.getContent();;

  var datum = {
    @if($is_draft)
      post_id:{{$posting->id}},
    @endif
      category_id:"{{$posting->category_id}}",
    	title: title,
    	content: content,
      cover :cover !== null ? cover : "",
      background:background,
    	attachment1:attach[0] !== null ? attach[0] : "",
    	attachment2:attach[1] !== null ? attach[1] : "",
    	attachment3:attach[2] !== null ? attach[2] : "",
      status:0,
      font:""
    };

    console.log(datum);

  $.ajax({
    @if($is_draft)
    url: '/nightblog/create/update',
    @else
    url: '/nightblog/create/insert',
    @endif
    type: 'POST', 
    data: datum,
    beforeSend: function(){
      $("#loader1").show();
    },
    success : function(response){
      $("#loader1").hide();
      console.log(response);
    
      alert(response.respMessage);
  
    },
  });
});

$(".draft").click(function(){
  title = $("input[name=title]").val();
  content = tinymce.activeEditor.getContent();;

  var datum = {
    @if($is_draft)
      post_id:{{$posting->id}},
    @endif
      category_id:"{{$posting->category_id}}",
    	title: title,
    	content: content,
      cover :cover !== null ? cover : "",
      background:background,
    	attachment1:attach[0] !== null ? attach[0] : "",
    	attachment2:attach[1] !== null ? attach[1] : "",
    	attachment3:attach[2] !== null ? attach[2] : "",
      status:1,
      font:""
    };

    console.log(datum);

  $.ajax({
    @if($is_draft)
    url: '/nightblog/create/update',
    @else
    url: '/nightblog/create/insert',
    @endif
    type: 'POST', 
    data: datum,
    beforeSend: function(){
      $("#loader1").show();
    },
    success : function(response){
      $("#loader1").hide();
      console.log(response);
    
      alert(response.respMessage);
  
    },
  });
});

$('#uploadCover').change(function(){
  var data = new FormData();
  data.append('image', $('#uploadCover').prop('files')[0]);
    
    $.ajax({
        xhr: function() {
          var xhr = new window.XMLHttpRequest();

          xhr.upload.addEventListener("progress", function(evt) {
            if (evt.lengthComputable) {
                percentComplete = evt.loaded / evt.total;
                percentComplete = parseInt(percentComplete * 100);
                console.log(percentComplete);
                showLoader();
                $("#content_loader").html('<i class="fa fa-spinner fa-4x fa-spin" id="loader1"></i> Uploading..('+percentComplete+'%)')
            }
          }, false);

          return xhr;
        },
        url: "/nightblog/post/upload/image",
        type: "post",
        dataType: 'json',
        processData: false,
        contentType: false,
        data: data,
        beforeSend: function(){
          showLoader();
        },
        success: function(data){
          $('#loaderModal').appendTo("body").modal('hide');
            console.log(data);
            cover = data.image;
            $("#cover-box").html("<img class=\"img-fluid\" style=\"height:100%\" src='"+data.url+"'/>");
            $(".fa-picture-o").hide();
        },
        error: function(xhr, status, error) {
          //var err = eval("(" + xhr.responseText + ")");
          //alert(err.Message);
          console.log(error);
        }
    });
});

function showModal(){
  $('#attachModal').appendTo("body").modal('show');
}

function showModalColor(cb){
  $('#colorPickerDialog').appendTo("body").modal('show');
  $(".bgcolor").click(function(){
    var color = $(this).val();
    cb(color);
    background = color;
    $('#colorPickerDialog').appendTo("body").modal('hide');
    console.log("color",color);  
  });

  $('#bgimage').change(function(){
  var data = new FormData();
  data.append('image', $('#bgimage').prop('files')[0]);
    
    $.ajax({
      xhr: function() {
          var xhr = new window.XMLHttpRequest();

          xhr.upload.addEventListener("progress", function(evt) {
            if (evt.lengthComputable) {
                percentComplete = evt.loaded / evt.total;
                percentComplete = parseInt(percentComplete * 100);
                console.log(percentComplete);
                showLoader();
                $("#content_loader").html('<i class="fa fa-spinner fa-4x fa-spin" id="loader1"></i> Uploading..('+percentComplete+'%)')
            }
          }, false);

          return xhr;
        },
        url: "/nightblog/post/upload/image",
        type: "post",
        dataType: 'json',
        processData: false,
        contentType: false,
        data: data,
        beforeSend: function(){
          showLoader();
        },
        success: function(data){
          $('#loaderModal').appendTo("body").modal('hide');
            console.log(data);
            //cover = data.url;
            background = data.url;
            cb("url('"+data.url+"')");
            $('#colorPickerDialog').appendTo("body").modal('hide');
        },
        error: function(xhr, status, error) {
          //var err = eval("(" + xhr.responseText + ")");
          //alert(err.Message);
          console.log(error);
        }
    });
});
  
}

function showLoader(){
  $('#loaderModal').appendTo("body").modal('show');
}
</script>

@endsection

 