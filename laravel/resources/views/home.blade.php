
    @extends('templates/template')

    @section('content')
    <style>
      .three-lines {
          -webkit-line-clamp: 3;
          display: -webkit-box;
          -webkit-box-orient: vertical;
          white-space: normal;
          overflow: hidden;
      }

      .two-lines {
          -webkit-line-clamp: 2;
          display: -webkit-box;
          -webkit-box-orient: vertical;
          white-space: normal;
          overflow: hidden;
      }

       .card-header {
          padding: .75rem 1.25rem;
          margin-bottom: 0;
          background-color: #fff;
          border-bottom: 0px solid rgba(0,0,0,.12);
      }

      .pl-3, .px-3 {
            padding-left: 10px!important;
        }
        .pb-3, .py-3 {
            padding-bottom: 0rem!important;
        }
        .pr-3, .px-3 {
            padding-right: 10px!important;
        }
        .pt-3, .py-3 {
            padding-top: 1rem!important;
        }

        /* .container {
            width: 100%;
            padding-right: 0px;
            padding-left: 0px;
            margin-right: auto;
            margin-left: auto;
        } */

        div.card-body{
          margin-top:0px;
        }
       
    </style>
    @if (!empty($res))
    <br>
    @foreach ($res as $res)
      <!-- Blog Entries Column -->
      <div class="col-md-2"></div>
      <div class="col-md-8">
 
        <!-- Blog Post -->
       
        <div class="card mb-4">
        <div class="card-header">
             <span style="float:left">
              @if($res->pic != "")
                <img onError="this.onerror=null;this.src='https://demo.sukowidodo.com/nightblog/public/my_template/image/logo.jpeg';" onclick="location.href='/nightblog/profile/{{$res->UserId}}/<?php echo session('id');?>';" src="{{ str_replace("http://res.cloudinary.com/nightblog/image/upload/w_300,h_300","http://res.cloudinary.com/nightblog/image/upload/w_300,h_300",$res->pic)}}" class="img img-circle" style="width:40px;height:40px;border-radius:50%"/> 
              @else
                <img src="{{ asset('my_template/image/logo.jpeg')}}" style="width:40px;height:40px;border-radius:50%" class="img img-circle" onclick="location.href='/nightblog/profile/{{$res->UserId}}/<?php echo session('id');?>';"/>
              @endif
            </span>
             <span style="float:left;margin-top:10px;margin-left:20px;color:#3297fc" ><b>{{$res->UserName}}</b></span>
            @if($res->IsBookmark==1)
              <span style="float:right;margin-top:10px;margin-left:10px;" onclick="bookMark({{$res->PostId}},true)"><i class="fa fa-bookmark fa-2x"></i></span>
            @else
              <span style="float:right;margin-top:10px;margin-left:10px;" onclick="bookMark({{$res->PostId}},false)"><i class="fa fa-bookmark-o fa-2x"></i></span>
            @endif
          </div>
        @if($is_draft)
          <div class="card-body" onclick="location.href='/nightblog/edit/{{$res->PostId}}';">
        @else
          <div class="card-body" onclick="location.href='/nightblog/post/detail/{{$res->PostId}}';">
        @endif

            <div class="row">
              @if ($res->Cover)
              <div class="col-8">
              @else
              <div class="col-12">
              @endif
                <h2 class="card-title two-lines" style="width:100%;margin-bottom:1px"><b>{{ $res->Title }}</b></h2>
                <p class="card-text" style="margin-bottom:1px"><b>{{ "(".$res->CategoryName.")" }}</b></p>
                <p class="three-lines"><?php echo string(strip_tags(htmlspecialchars_decode($res->Content), '<p>'))->tease(500); ?></p>
              </div>
              @if ($res->Cover)
              <div class="col-4">
                  
                  <img onclick="location.href='/nightblog/post/detail/{{$res->PostId}}';" class="card-img-top thumb-post img-fluid" src="{{"http://res.cloudinary.com/nightblog/image/upload/w_300,h_300,c_fill/".$res->Cover}}" styles="height:100px;">
                  {{-- @else
                  <img onclick="location.href='/nightblog/post/detail/{{$res->PostId}}';" class="card-img-top thumb-post img-fluid" src="http://placehold.it/750x300" styles="height:100px;"> --}}
               
              </div>
              @endif
            </div>
          </div>
          <div class="card-footer">
            @if($res->IsLove == 1)
              <i class="fa fa-heart text-danger" aria-hidden="true" onclick="loveUnlove({{$res->PostId}},true)"></i> 
            @else
              <i class="fa fa-heart-o" aria-hidden="true" onclick="loveUnlove({{$res->PostId}},false)"></i>
            @endif
            {{$res->LoveCount}} <img src="{{asset('my_template/image/ic_comment_custom.png')}}" style="width:14px;height:14px"/> {{$res->CommentCount}}
            <?php
              $format = 'Y-m-d H:i:s';
              $date = DateTime::createFromFormat($format, $res->PublishDate);
              $date = $date->format('j M Y');
            ?>
          <span style="float:right;margin-left:10px;"><i class="fa fa-clock-o" aria-hidden="true"></i> {{$date}}</span>
          </div>
        </div>
      </div>
      <div class="col-md-2"></div>
      @endforeach
      @else
       <div class="col-md-2"></div>
      <div class="col-md-8">
        <!-- Blog Post -->
        <div class="card mb-4">
          <div class="card-body">
            <p class="card-text">there is no data</p>
          </div>
        </div>
      </div>
      <div class="col-md-2"></div>
      @endif
      {{-- <div class="container" style="text-align:center">
      <button class="btn btn-outline-danger" onclick="loadMore()">More</button>
      </div> --}}
      <script>
        function loveUnlove(id,isLove){
          console.log(id+"---"+isLove);
          var datum = {
              id:id,
              isLove:isLove
            };
          $.ajax({
            url: '/nightblog/post/love_unlove',
            type: 'POST', 
            data: datum,
            success : function(response){
              console.log(response);
              document.location.reload(true);
            },
          });
        }

        function bookMark(id,is_bookmark){
          console.log(id+"---"+is_bookmark);
          var datum = {
            post_id:id,
            is_bookmark:is_bookmark
            };
          $.ajax({
            url: '/nightblog/post/set_bookmark',
            type: 'POST', 
            data: datum,
            success : function(response){
              console.log(response);
              if(response.err){
                console.log(response.respMessage);
              }else{
                document.location.reload(true);
              }
            },
          });
        }
        var next = 1;
        function loadMore(){
          var datum = {
              page:next++
            };
          $.ajax({
            url: '/nightblog/home2',
            type: 'POST', 
            data: datum,
            success : function(response){
              console.log(response);
              for(i=0;i<response.length;i++){
                $(".row").html(response[i]);
              }
            },
          });
        }



        
      </script>

  @endsection

