<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>Nightblog</title>

<!-- Bootstrap core CSS -->
<link href={{ asset('my_template/vendor/bootstrap/css/bootstrap.min.css' )}} rel="stylesheet">


<script src="https://use.fontawesome.com/245b0214e2.js"></script>

<!-- Custom styles for this template -->
<link href={{ asset('my_template/css/blog-home.css' )}} rel="stylesheet">
<style>
body {
    background-color: #fdfdfd;
    background-image: url("data:image/svg+xml,%3Csvg width='40' height='40' viewBox='0 0 40 40' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M0 0h20v20H0V0zm10 17a7 7 0 1 0 0-14 7 7 0 0 0 0 14zm20 0a7 7 0 1 0 0-14 7 7 0 0 0 0 14zM10 37a7 7 0 1 0 0-14 7 7 0 0 0 0 14zm10-17h20v20H20V20zm10 17a7 7 0 1 0 0-14 7 7 0 0 0 0 14z' fill='%23e8e8e8' fill-opacity='0.4' fill-rule='evenodd'/%3E%3C/svg%3E");
}
</style>

</head>

<body>


  <!-- Page Content -->
  <div class="container" >
    <div class="row">
      <!-- Blog Entries Column -->
      <div class="col-md-3"></div>
      <div class="col-md-6" style="margin-top:-60px">
        <!-- Blog Post -->
         <div class="card mb-4">
          <img class="card-img-top thumb-post img-fluid" src="{{ asset('my_template/image/nb.png')}}" styles="height:100px;">
          <div class="card-body">
            <div class="card" style="margin-top:-50px">
                <div class="card-body">
                    <form>
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" id="exampleInputPassword1" placeholder="Full Name">
                        </div>
                        <div class="form-group">
                            <input type="text" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Username">
                        </div>
                        <div class="form-group">
                            <input type="text" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                        </div>
                        <button type="submit" class="btn-submit btn btn-primary" style="width:100%;background:#000000">Submit</button>
                    </form>
                </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3"></div>
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->

  <!-- Bootstrap core JavaScript -->
  <script src="{{ asset('my_template/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{ asset('my_template/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  <script type="text/javascript">
    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });
    $(".btn-submit").click(function(e){
        e.preventDefault();
        var name = $("input[name=name]").val();
        var password = $("input[name=password]").val();
        var email = $("input[name=email]").val();
        var username = $("input[name=username]").val();
        $.ajax({
           type:'POST',
           url:'/nightblog/do_register',
           data: {
             name:name, 
             password:password,  
             email:email,
             username:username
            },
           success:function(data){
              console.log(data);
               if(data.err==false){
                  window.location.replace("/nightblog/home");
              }else{
                  alert(data.errMsg);
              }
           }
        });
      });

</script>

</body>

</html>
