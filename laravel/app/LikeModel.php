<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikeModel extends Model
{
    protected $table = 'tbl_post_like';
    public $timestamps=false;
}
