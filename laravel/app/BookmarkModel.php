<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookmarkModel extends Model
{
    protected $table = 'tbl_bookmark';
    public $timestamps=false;
}
