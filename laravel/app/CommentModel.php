<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentModel extends Model
{
    protected $table = 'tbl_post_comment';
    public $timestamps=false;
    
}
