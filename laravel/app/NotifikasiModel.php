<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotifikasiModel extends Model
{
    protected $table = 'tbl_notifikasi';
    public $timestamps=false;
}
