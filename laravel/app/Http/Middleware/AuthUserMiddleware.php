<?php

namespace App\Http\Middleware;

use Closure;

use App\UserModel;
use DB;

class AuthUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->cookie('auth_token');
        $islogin = DB::table("tbl_user")->where("auth_token","=",$token)->get();
        //return response()->json($islogin);
        if(empty($islogin[0])){
            return redirect('landing?logout=true');
        }else{
            session([
                "id"=>$islogin[0]->id,
                "username"=>$islogin[0]->password,
                "name"=>$islogin[0]->name,
                "email"=>$islogin[0]->email,
                "pic"=>$islogin[0]->pic
            ]);
            return $next($request);
        }
        
    }
}
