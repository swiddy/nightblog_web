<?php

namespace App\Http\Middleware;

use Closure;

use App\NotifikasiModel;
use DB;

class ReadNotifMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $notif = NotifikasiModel::where("id_user","=",session("id"))->update(['is_read' => 1]);;
        return $next($request);
    
    }
}
