<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client as Client;

use Illuminate\Http\Request;

use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

use JD\Cloudder\Facades\Cloudder;

use App\PostModel;
use App\CommentModel;
use App\UserModel;
use App\LikeModel;
use App\BookmarkModel;

use DB;

class PostController extends Controller
{
    public function home(Request $request){
        $page = $request->input("page");
        try{
            $url = env("API_BASE","https://api.sukowidodo.com/ws_nightblog/")."item_list.php";
            $client = new Client(); 
            $result = $client->request('GET',$url, [
                'query' => [
                    'ct' => 'ITEM_POST_PUBLISH',
                    'UserId' => $request->session()->get('id', NULL),
                    'Status'=>0,
                    'currentpage'=>1
                ]
            ]);
            $data = $result->getBody();
            $res = json_decode($data);
            $is_draft = false;
            return view('home',compact('res','is_draft'));
        }catch (RequestException $e) {
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
    }

    public function draft(Request $request){
        try {
            $UserId = $request->session()->get('id', NULL);
            $sql = "SELECT 
			A.id as PostId, 
			A.category_id as CategoryId, 
			B.id as UserId, 
			A.status as Status, 
			C.name as CategoryName, 
			B.name as UserName, 
			B.password as UserPass, 
			B.username, 
			B.pic,
			B.lokasi,
			B.profesi,
			B.islocked, 
			A.title as Title, 
			A.content as Content, 
			A.cover as Cover, 
			A.background,
			A.font,
			A.attachment1, 
			A.attachment2, 
			A.attachment3, 
			A.publish_date as PublishDate, 
			count(A.id),
			(SELECT id FROM tbl_bookmark E WHERE A.id = E.post_id AND E.user_id = ".$UserId." LIMIT 1) as PostBookmarkId, 
			(SELECT is_bookmark FROM tbl_bookmark E WHERE A.id = E.post_id AND E.user_id = ".$UserId." LIMIT 1) as IsBookmark, 
			(SELECT id FROM tbl_post_like D WHERE A.id = D.post_id AND D.user_id = ".$UserId." LIMIT 1) as PostLoveId, 
			(SELECT is_love FROM tbl_post_like D WHERE A.id = D.post_id AND D.user_id = ".$UserId." LIMIT 1) as IsLove, 
			(SELECT count(Id) FROM tbl_post_like D WHERE A.id = D.post_id AND D.is_love=1 LIMIT 1) as LoveCount, 
			(SELECT count(Id) FROM tbl_post_comment D WHERE A.id = D.post_id LIMIT 1) as CommentCount 
			FROM tbl_post A, tbl_user B, tbl_category C, tbl_followers D 
			where A.user_id = B.id 
			AND A.category_id = C.id 
			AND A.user_id = ".$UserId."
			AND A.status = '1' 
			GROUP BY A.id 
            ORDER BY A.id DESC";

            $res = DB::select($sql);
            
            // $url = env("API_BASE","https://api.sukowidodo.com/ws_nightblog/")."item_list.php";
            // $client = new Client(); 
            // $result = $client->request('GET',$url, [
            //     'query' => [
            //         'ct' => 'ITEM_POST_PUBLISH',
            //         'UserId' => $request->session()->get('id', NULL),
            //         'Status'=>1,
            //         'currentpage'=>0
            //     ]
            // ]);
            // $data = $result->getBody();
            // $res = json_decode($data);
             $is_draft = true;
            return view('home',compact('res','is_draft'));
        }catch (RequestException $e) {
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
    }

    public function TopPost(Request $request){
        try{
            $url = env("API_BASE","https://api.sukowidodo.com/ws_nightblog/")."item_list.php";
            $client = new Client(); 
            $result = $client->request('GET',$url, [
                'query' => [
                    'ct' => 'ITEM_POST_TOP',
                    'UserId' => $request->session()->get('id', NULL),
                    'Status'=>1,
                    'currentpage'=>0
                ]
            ]);
            $data = $result->getBody();
            $res = json_decode($data);

            $url2 = env("API_BASE","https://api.sukowidodo.com/ws_nightblog/")."item_list.php";
            $client2 = new Client(); 
            $payload = [
                'form_params' => [
                    'ct' => 'ITEM_POST_PEOPLE'
                ]
            ];
            $result2 = $client2->request('POST',$url2,$payload );
            $data2 = $result2->getBody();
            $res2 = json_decode($data2);
            //return response()->json($res2);
            
            return view('top',compact('res','res2'));
        }catch (RequestException $e) {
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
    }

    public function getBookmarkPost(Request $request){
        try {
            $res = [];
            $url = env("API_BASE","https://api.sukowidodo.com/ws_nightblog/")."item_list.php";
            $client = new Client(); 
            $result = $client->request('GET',$url, [
                'query' => [
                    'ct' => 'ITEM_POST_BOOKMARK',
                    'UserId' => session("id")
                ]
            ]);
            $data = $result->getBody();
            $res = json_decode($data);
            $is_draft = false;
            return view('home',compact('res','is_draft'));
        }catch (RequestException $e) {
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
    }

    public function detailPost(Request $request){
        $id = $request->id;
        $post = PostModel::find($id);
        $user = UserModel::find($post->user_id);
        $is_bookmark = BookmarkModel::where("user_id","=",session('id'))
                ->where("post_id","=",$id)
                ->where("is_bookmark","=",1)
                ->count();
        $comment = DB::table("tbl_post_comment")
                ->join("tbl_user","tbl_user.id","=","tbl_post_comment.user_id")
                ->where("post_id","=",$id)
                ->get();
    
        //return $comment;
        $like = LikeModel::where("post_id","=",$id)->get();
        $isLovex = LikeModel::where("user_id","=",session('id'))
        ->where("post_id","=",$id)
        ->get();
        $isLove = count($isLovex) > 0 ? true : false;
        return view('post',compact('post','user','comment','like','isLove','is_bookmark'));
    }

    public function editPost(Request $request){
        $id = $request->id;
        $posting = PostModel::find($id);
        $user = UserModel::find($posting->user_id);
        $is_bookmark = BookmarkModel::where("user_id","=",$posting->user_id)->where("post_id","=",$id)->where("is_bookmark","=",1)->count();
        $comment = DB::table("tbl_post_comment")
        ->join("tbl_user","tbl_user.id","=","tbl_post_comment.user_id")
        ->where("post_id","=",$id)
        ->get();
    
        //return $comment;
        $like = LikeModel::where("post_id","=",$id)->get();
        $isLovex = LikeModel::where("user_id","=",session('id'))
        ->where("post_id","=",$id)
        ->get();
        $isLove = count($isLovex) > 0 ? true : false;
        $is_draft = true;
        return view('create',compact('posting','user','comment','like','isLove','is_bookmark','is_draft'));
    }

    public function createPost(Request $request){
        $posting = new \stdClass;
        $posting->background = "";
        switch($request->kategori){
            case "ulasan":
                $posting->category_id = 1;
                break;
            case "karya_tulis":
                $posting->category_id = 3;
                break;
        }
        $is_draft = false;
        return view('create',compact('posting','is_draft'));
    }

    public function addComment(Request $request){
        try {
            $post_id = $request->input("post_id");
            $user_id = $request->input("user_id");
            $comment = $request->input("comment");

        //     $insert = new CommentModel;
        //     $insert->post_id = $post_id;
        //     $insert->user_id = $user_id;
        //     $insert->comment = $comment;
        //     $insert->save();

        //     $koment = DB::table("tbl_post_comment")->join("tbl_user","tbl_user.id","=","tbl_post_comment.user_id")->where("tbl_post_comment.id","=",$insert->id)
        // ->get()->first();

            
            $url = env("API_BASE","https://api.sukowidodo.com/ws_nightblog/")."item_add.php";
            $client = new Client(); 
            $result = $client->request('POST',$url, [
                'form_params' => [
                    'ct' => 'ITEM_POST_COMMENT',
                    'post_id'=> $post_id,
    	            'user_id'=> $user_id,
                    'comment'=> $comment,
                ]
            ]);
            $data = $result->getBody();
            $res = json_decode($data);
            if($res->errorcode===0){
                return response()->json(array(
                    "err"=>false,
                    "respMessage"=>"Berhasil disimpan",
                    "comment"=>$comment
                ));
            }else{
                return response()->json(array(
                    "err"=>true,
                    "respMessage"=>"",
                    "comment"=>$comment
                ));
            }

        }catch(\Exception $e){
            return response()->json(array(
                "err"=>true,
                "respMessage"=>"Error ".$e->getMessage()
            ));
        }
    }

    public function savePost(Request $request){
        try{
            $category_id = $request->input('category_id');
            $user_id     = session('id');
            $title       = $request->input('title');
            $content     = $request->input('content');
            $cover       = $request->input('cover');
            $background  = $request->input('background');
            $attachment1 = $request->input('attachment1');
            $attachment2 = $request->input('attachment2');
            $attachment3 = $request->input('attachment3');
            $status      = $request->input('status');
            $font        = $request->input('font');


            $post = new PostModel;
            $post->category_id = $category_id;
            $post->user_id = $user_id;
            $post->title = $title;
            $post->content = $content;
            $post->cover = $cover;
            $post->background = $background;
            $post->attachment1 = $attachment1;
            $post->attachment2 = $attachment2;
            $post->attachment3 = $attachment3;
            $post->status = $status;
            $post->publish_date = date("Y-m-d H:i:s");
            $post->font = $font;
            $post->save();
            
            return array(
                "err" => false,
                "respMessage"=>"Sukses disimpan"
            );
        }catch(\Exception $e){
            return array(
                "err" => true,
                "respMessage"=>$e->getMessage()
            );
        }
    }

    public function updatePost(Request $request){
        try{
            $post_id     = $request->input('post_id');
            $category_id = $request->input('category_id');
            $user_id     = session('id');
            $title       = $request->input('title');
            $content     = $request->input('content');
            $cover       = $request->input('cover');
            $background  = $request->input('background');
            $attachment1 = $request->input('attachment1');
            $attachment2 = $request->input('attachment2');
            $attachment3 = $request->input('attachment3');
            $status      = $request->input('status');
            $font        = $request->input('font');


            $post = PostModel::find($post_id);
            $post->category_id = $category_id;
            $post->user_id = $user_id;
            $post->title = $title;
            $post->content = $content;
            $post->cover = $cover;
            $post->background = $background;
            $post->attachment1 = $attachment1;
            $post->attachment2 = $attachment2;
            $post->attachment3 = $attachment3;
            $post->status = $status;
            $post->publish_date = date("Y-m-d H:i:s");
            $post->font = $font;
            $post->save();
            
            return array(
                "err" => false,
                "respMessage"=>"Sukses disimpan"
            );
        }catch(\Exception $e){
            return array(
                "err" => true,
                "respMessage"=>$e->getMessage()
            );
        }
    }

    public function uploadAttachment(Request $request){

        $res = [];

        //return $attachment1;
        //$result = \Cloudinary\Uploader::upload($attachment1, $options = array());
        if($request->file('attachment1') != NULL){
            $attachment1 = $request->file('attachment1')->getRealPath();
            Cloudder::upload($attachment1,  time().".".$request->file('attachment1')->getClientOriginalExtension(), array("resource_type" => "raw"));
            $image_url1 = Cloudder::getResult();
            array_push($res,$image_url1['secure_url']);
        }
        
        if($request->file('attachment2') != NULL){
            $attachment2 = $request->file('attachment2')->getRealPath();
            Cloudder::upload($attachment2,  time().".".$request->file('attachment2')->getClientOriginalExtension(), array("resource_type" => "raw"));
            $image_url2 = Cloudder::getResult();
            array_push($res,$image_url2['secure_url']);
        }

        if($request->file('attachment3') != NULL){
            $attachment3 = $request->file('attachment3')->getRealPath();
            Cloudder::upload($attachment3,  time().".".$request->file('attachment3')->getClientOriginalExtension(), array("resource_type" => "raw"));
            $image_url3 = Cloudder::getResult();
            array_push($res,$image_url3['secure_url']);
        }

        return response()->json($res);
    }

    public function uploadImage(Request $request){
        try{
            if($request->file('image') != NULL){
                $image = $request->file('image')->getRealPath();
                $name = time();
                Cloudder::upload($image, $name ,array("width"=>700, "height"=>700, "crop"=>"pad"));
                $image_url1 = Cloudder::getResult();
                //return $image_url1;
                return array(
                    "err" => false,
                    "url" => $image_url1['secure_url'],
                    "image"=>"v".$image_url1['version']."/".$name.".".$request->file('image')->getClientOriginalExtension()
                );
            }
        }catch(\Exception $e){
            return array(
                "err" => true,
                "respMessage" => $e->getMessage()
            );
        }
    }

    public function loveUnlove(Request $request){
        try{
            DB::beginTransaction();
            $id = $request->input("id");
            $isLove = $request->input("isLove");
            $tes = "";

            if($isLove!="true"){
                $url = env("API_BASE","https://api.sukowidodo.com/ws_nightblog/")."item_add.php";
                $client = new Client(); 
                $result = $client->request('POST',$url, [
                    'form_params' => [
                        'ct' => 'ITEM_POST_LOVE',
                        'post_id'=> $id,
                        'user_id'=> session("id"),
                        'is_love'=> 1,
                    ]
                ]);
                $data = $result->getBody();
                $res = json_decode($data);
                return response()->json($res);
            }else{
                $tes="true";
                $like = LikeModel::where("post_id","=",$id)
                ->where("user_id","=",session("id"))
                ->delete();
            }
            DB::commit();
            return response()->json(array(
                "err"=>false,
                "respMessage"=>"Berhasil ".$isLove
            ));
            
        }catch(\Exception $e){
            DB::rollBack();
            return response()->json(array(
                "err"=>true,
                "respMessage"=>"Error ".$e->getMessage()
            ));
        }
    }

    public function bookmarkUnbookmark(Request $request){
        try {
            DB::beginTransaction();
            $is_bookmark = $request->input("is_bookmark");
           // $user_id = $request->input("user_id");
            $post_id = $request->input("post_id");
            // return response()->json(array(
            //     "is_bookmark"=>$is_bookmark,
            //     "post_id"=>$post_id
            // ));

            if($is_bookmark != "true"){
                $check_again = BookmarkModel::where("post_id","=",$post_id)->where("user_id","=",session("id"))->get();
                if(!count($check_again)>0){
                    $book = new BookmarkModel;
                    $book->post_id = $post_id;
                    $book->user_id = session("id");
                    $book->is_bookmark = 1;
                    $book->save();
                }else{
                    return response()->json(array(
                        "err"=>true,
                        "respMessage"=>"Already bookmarked"
                    ));
                }
            }else{
                $book = BookmarkModel::where("post_id","=",$post_id)
                ->where("user_id","=", session("id"))
                ->delete();
            }
            DB::commit();
            return response()->json(array(
                "err"=>false,
                "respMessage"=>"Berhasil ".$post_id
            ));
            

        }catch(\Exception $e){
            DB::rollBack();
            return response()->json(array(
                "err"=>true,
                "respMessage"=>"Error ".$e->getMessage()
            ));
        }
        
    }

    public function deletePost(Request $request){
        try{
            DB::beginTransaction();
            $id = $request->input("id");
            $post = PostModel::find($id);
            $post->delete();
            DB::commit();
            return response()->json(array(
                "err"=>false,
                "respMessage"=>"Sukses dihapus"));
        }catch(\Exception $e){
            DB::rollBack();
            return response()->json(array(
                "err"=>true,
                "respMessage"=>"Error ".$e->getMessage()
            ));
        }
        
    }
}
