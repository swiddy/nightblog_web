<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NotifikasiModel;
use App\PostModel;
use App\UserModel;
use App\FollowModel;

use DB;

use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client as Client;

class ProfileController extends Controller
{
    public function index(Request $request){

        $UserId = $request->UserId;
        $ViewerId = $request->ViewerId;

        $karya = [];
        $ulasan = [];
        $isFollow;

        try {
            $url = env("API_BASE","https://api.sukowidodo.com/ws_nightblog/")."item_list.php";

            $client = new Client(); 
            $ulasan1 = $client->request('GET',$url, [
                'query' => [
                    'ct' => 'ITEM_PROFILE_POST',
                    'UserId' => $UserId,
                    'ViewerId'=> $ViewerId,
                    'CategoryId'=>1,
                    'currentpage'=>0
                ]
            ]);
        
            $ulasan = empty(json_decode($ulasan1->getBody())) ? [] : json_decode($ulasan1->getBody());

            $karya1 = $client->request('GET',$url, [
                'query' => [
                    'ct' => 'ITEM_PROFILE_POST',
                    'UserId' => $UserId,
                    'ViewerId'=> $ViewerId,
                    'CategoryId'=>3,
                    'currentpage'=>0
                ]
            ]);

            $karya = empty(json_decode($karya1->getBody())) ? [] : json_decode($karya1->getBody());

            $profile1 = $client->request('GET',$url, [
                'query' => [
                    'ct' => 'ITEM_PROFILE',
                    'UserId' => $UserId,
                    'FollowerId'=> $ViewerId
                ]
            ]);

            $profile2 = empty(json_decode($profile1->getBody())) ? [] : json_decode($profile1->getBody());
            $profile = $profile2[0];

            //return response()->json(array("karya"=>$profile));

            $is_follow = FollowModel::where("user_id","=",$UserId)->where("follower_id","=",$ViewerId)->get()->first();
            if($UserId != $ViewerId){
                $isFollow = empty($is_follow) ? 0 : 1;
            }else{
                $isFollow = 3;
            }

            return view('profile',compact('karya','ulasan','file','profile','isFollow'));

        }catch (RequestException $e) {
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
    }

    public function notifikasi(Request $request){
        $notif = DB::table("tbl_notifikasi")
        ->select("*")
        ->join("tbl_user","tbl_user.id","=","tbl_notifikasi.id_client")
        ->where("tbl_notifikasi.id_user", $request->session()->get('id', NULL))->get()->map(function($e){
            if(empty($e->pic)){
                if ($e->password == "3667b4d8fffc4a51b5fa71ff226e4181") {
                    $e->pic = "https://graph.facebook.com/".$e->username."/picture?type=large";
                } else if ($e->password == "b49179f14eb706bfeb6a18bb771b493f") {
                    $e->pic = $e->username;
                }else{
                    // if(!empty($e->pic)){
                        $e->pic = "https://demo.sukowidodo.com/nightblog/public/my_template/image/logo.jpeg";
                    // }else{
                    //     $e->pic = $e->pic;
                    // }
                }
            }else if(substr($e->pic,0,4)=="http"){
                $e->pic = $e->pic;
            }   
            return $e;
        });
        return view('notif',compact('notif'));
    }

    public function notifikasi2(Request $request){
        
        
        $response = new \Symfony\Component\HttpFoundation\StreamedResponse(function() use ($request) {
            while(true) {
                $notif = DB::table("tbl_notifikasi")
                ->join("tbl_user","tbl_user.id","=","tbl_notifikasi.id_client")
                ->where("tbl_notifikasi.is_read","=",0)
                ->where("tbl_notifikasi.id_user", $request->session()->get('id', NULL))->get()->map(function($e){
                    if ($e->password == "3667b4d8fffc4a51b5fa71ff226e4181") {
                        $e->pic = "https://graph.facebook.com/".$e->username."/picture?type=large";
                    } else if ($e->password == "b49179f14eb706bfeb6a18bb771b493f") {
                        $e->pic = $e->username;
                    }else{
                        $e->pic = "https://demo.sukowidodo.com/nightblog/public/my_template/image/logo.jpeg";
                    }
                    return $e;
                });
                echo 'data: ' . json_encode($notif) . "\n\n";
                ob_flush();
                flush();
                usleep(200000);
            }
        });
        $response->headers->set('Content-Type', 'text/event-stream');
        $response->headers->set('X-Accel-Buffering', 'no');
        $response->headers->set('Cach-Control', 'no-cache');
        return $response;
    }

    public function truncateNotif(Request $request){
        try {
            $hapus = NotifikasiModel::where("id_user","=",$request->session()->get('id', NULL))->delete();
            return array("err"=>false,"respMessage"=>"Berhasil dihapus");
        }catch(\Exception $e){
            return array("err"=>true,"respMessage"=>"error ".$e->getMessage());
        }
        
    }

    public function help(Request $request){
        return view('help');
    }

    public function search(){
        return view('cari');
    }

    public function edit(){
        $user = UserModel::find(session("id"));
        return view('edit_profile',compact("user"));
    }

    public function follower(Request $request){
        $id = $request->id;
        $url = env("API_BASE","https://api.sukowidodo.com/ws_nightblog/")."item_list.php";

        $client = new Client(); 
        $response = $client->request('GET',$url, [
            'query' => [
                'ct' => 'ITEM_PROFILE_FOLLOWERS',
                'UserId' => $id
            ]
        ]);
    
        $data = empty(json_decode($response->getBody())) ? [] : json_decode($response->getBody());
        //return response()->json($data);
        return view('follow',compact("data"));
    }

    public function following(Request $request){
        $id = $request->id;
        $url = env("API_BASE","https://api.sukowidodo.com/ws_nightblog/")."item_list.php";

        $client = new Client(); 
        $response = $client->request('GET',$url, [
            'query' => [
                'ct' => 'ITEM_PROFILE_FOLLOWING',
                'UserId' => $id
            ]
        ]);
    
        $data = empty(json_decode($response->getBody())) ? [] : json_decode($response->getBody());
        //return response()->json($data);
        return view('follow',compact("data"));
    }

    public function follow(Request $request){
        $user_id = $request->input("user_id");
        $follower_id = $request->input("follower_id");
        $is_follow = $request->input("is_follow");
        try {
            if($is_follow=="true"){
                $url = env("API_BASE","https://api.sukowidodo.com/ws_nightblog/")."item_add.php";
                $client = new Client(); 
                $response = $client->request('POST',$url, [
                    'form_params' => [
                        'ct' => 'ITEM_PROFILE_FOLLOW',
                        'user_id' => $user_id,
                        'follower_id'=>$follower_id
                    ]
                ]);
            
                $data = empty(json_decode($response->getBody())) ? [] : json_decode($response->getBody());
                return response()->json($data);
            }else{
                $x = FollowModel::where("user_id",$user_id)->where("follower_id",$follower_id)->delete();
                if($x){
                    return response()->json(array(
                        "err"=>false,
                        "respMessage"=>"Berhasil di unfollow"
                    ));
                }else{
                    return response()->json(array(
                        "err"=>true,
                        "respMessage"=>"gagal di unfollow"
                    ));
                }
            }
        }catch(\Exception $e){
            return response()->json(array(
                "err"=>true,
                "respMessage"=>$e->getMessage()
            ));
        }
    }

    public function update(Request $request){
        try{
            $nama = $request->input("nama");
            $profesi = $request->input("profesi");
            $kota = $request->input("kota");
            $foto = $request->input("foto");
            $locked = $request->input("locked");

            $user = UserModel::find(session("id"));
            $user->name = $nama;
            $user->profesi = $profesi;
            $user->lokasi = $kota;
            $user->islocked=$locked;
            
            if($foto != NULL){
                $user->pic = $foto;
                session(["pic"=>$foto]);
            }
            
            $user->save();
            return response()->json(array(
                "err"=>false,
                "user"=>$user,
                "respMessage"=>"Berhasil disimpan"
            ));
        }catch(\Exception $e){
            return response()->json(array(
                "err"=>true,
                "respMessage"=>$e->getMessage()
            ));
        }
    }

    public function do_search(Request $request){
        try{
            $query = $request->input("query");
            $user = UserModel::where("name","like","%".$query."%")->get()->map(function($e){
                if(empty($e->pic)){
                    if ($e->password == "3667b4d8fffc4a51b5fa71ff226e4181") {
                        if(!empty($e->username)){
                            $e->pic = "https://graph.facebook.com/".$e->username."/picture?type=large";
                        }else{
                            $e->pic = "https://demo.sukowidodo.com/nightblog/public/my_template/image/logo.jpeg";
                        }
                    } else if ($e->password == "b49179f14eb706bfeb6a18bb771b493f") {
                        if(substr($e->username,0,4)=="http"){
                            $e->pic = $e->username;
                        }else{
                            $e->pic = "https://demo.sukowidodo.com/nightblog/public/my_template/image/logo.jpeg";
                        }
                    }
                    else{
                        $e->pic = "https://demo.sukowidodo.com/nightblog/public/my_template/image/logo.jpeg";
                    }
                }else{
                    $e->pic = $e->pic;
                }
                
                return $e;
            });
            return response()->json($user);
        }catch(\Exception $e){
            return response()->json(array(
                "err"=>true,
                "respMessage"=>$e->getMessage()
            ));
        }
        
    }
}
