<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserModel;
use DB;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function login()
    {
        return view('login');
    }

    public function register()
    {
        return view('register');
    }

    public function do_login(Request $request){
        $username = $request->input("username");
        $password = $request->input("password");
        //return array($username,$password);

        $user = UserModel::where("username","=",$username)->get();
        //return $user;
        if(!empty($user[0])){
            if(md5($password) == $user[0]->password){
                $user[0]->auth_token = Str::random(60);
                $user[0]->save();

                session([
                    "id"=>$user[0]->id,
                    "username"=>$user[0]->password,
                    "name"=>$user[0]->name,
                    "email"=>$user[0]->email,
                    "pic"=>$user[0]->pic
                ]);
                $cookie = cookie()->forever('auth_token', $user[0]->auth_token);
                return redirect('home')->withCookie($cookie);
            }else{
                return response()->json(
                    array(
                        "err"=>true,
                        "errMsg"=>"Username or Password wrong"
                    )
                );
            }
        }else{
            return response()->json(
                array(
                    "err"=>true,
                    "errMsg"=>"User not yet regitered"
                )
            );
        }
    }

    public function do_register(Request $request){
        $username = $request->input("username");
        $password = $request->input("password");
        $name = $request->input("name");
        $email = $request->input("email");
        DB::beginTransaction();
        try {
            $user1 = UserModel::where("username","=",$username)->get();
            if(!empty($user1[0])){
                return response()->json(
                    array(
                        "err"=>true,
                        "errMsg"=>"Username is already registed"
                    )
                );
            }

            $user2 = UserModel::where("email","=",$email)->get();
            if(!empty($user2[0])){
                return response()->json(
                    array(
                        "err"=>true,
                        "errMsg"=>"Email is already registered"
                    )
                );
            }

            $user = new UserModel;
            $user->email = $email;
            $user->name = $name;
            $user->username = $username;
            $user->password = md5($password);
            $user->save();
            DB::commit();

            session([
                "id" => $user->id,
                "username" => $user->password,
                "name" => $user->name,
                "email" => $user->email
            ]);

            return response()->json(
                array(
                    "err" => false,
                    "respMsg" => "Success registered"
                )
            );
        }catch(\Exception $e){
            DB::rollBack();
            return response()->json(
                array(
                    "err"=>true,
                    "errMsg"=>"Email is already registered"
                )
            );
        }
    }

    public function do_logout(Request $request){
        try{
            $user = UserModel::find(session("id"));
            $user->auth_token = "";
            $user->save();
            $request->session()->flush();

            return response()->json(
                array(
                    "err" => false,
                    "respMsg" => "Success logout"
                )
            );

        }catch(\Exception $e){
            DB::rollBack();
            return response()->json(
                array(
                    "err"=>true,
                    "errMsg"=>$e
                )
            );
        }
    }

    public function landing(Request $request){
        $islogout = $request->logout;
        return view('landing',compact("islogout"));
    }

    public function loginWithSocial(Request $request){
        $email = $request->input("email");
        $type = $request->input("type");
        $id = $request->input("id");
        $name = $request->input("name");

        try {
            $user = UserModel::where("email","=",$email)->get()->first();

            if(!empty($user)){
                $user->auth_token = Str::random(60);
                $user->save();
    
                if($type == "fb"){
                    session([
                        "id"=>$user->id,
                        "username"=>$user->password,
                        "email"=>$user->email,
                        "name"=>$user->name,
                        "pic"=>$user->pic
                    ]);
                }else if($type == "gg"){
                    session([
                        "id"=>$user->id,
                        "username"=>$user->password,
                        "name"=>$user->name,
                        "email"=>$user->email,
                        "pic"=>$user->pic
                    ]);
                }

                $cookie = cookie()->forever('auth_token', $user->auth_token);
                return redirect('home')->withCookie($cookie);
            }else{
                $user = new UserModel;
                $user->email = $email;
                $user->name = $name;
                $user->pic = "";
                if($type=="fb"){
                    $user->password = \md5("FACEBOOK");
                }else if($type=="gg"){
                    $user->password = \md5("GMAIL");
                }
                $user->save();
                session([
                    "id"=>$user->id,
                    "username"=>$user->password,
                    "email"=>$user->email,
                    "name"=>$user->name,
                    "pic"=>$user->pic
                ]);
    
                $cookie = cookie()->forever('auth_token', $user->auth_token);
                return redirect('home')->withCookie($cookie);
            }
        } catch(\Exception $e){
            DB::rollBack();
            return response()->json(
                array(
                    "err"=>true,
                    "errMsg"=>$e
                )
            );
        }
    }
}
