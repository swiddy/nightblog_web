<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FollowModel extends Model
{
    protected $table = "tbl_followers";
    public $timestamps = false;
}
